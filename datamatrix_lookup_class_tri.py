# -*- coding: utf-8 -*-
"""
Created on Tue Aug  3 13:44:50 2021

@author: Joel Wulff
"""

# -*- coding: utf-8 -*-

import sys
import numpy as np
import os
import matplotlib.pyplot as plt
from collections import defaultdict
from scipy import interpolate as interp
import pickle
import time

SAVE_PATH = r"H:\user\j\jwulff\SWAN_projects\batch_jobs2\outputs\datamatrix_files\tri\assembled_lookup_tables\quad_datamatrix"
#DATA_PATH = r"\\eoshome-smb\eos\user\j\jwulff\SWAN_projects\batch_jobs2\outputs\datamatrix_files\tri"
#DATA_PATH = r"H:\user\j\jwulff\SWAN_projects\batch_jobs2\outputs\datamatrix_files\tri\machine_settings"
#DATA_PATH = r"\\cernbox-smb\eos\user\j\jwulff\SWAN_projects\batch_jobs2\outputs\datamatrix_files\tri\machine_settings_fixed" #ref_h14_refvf998"
DATA_PATH = r"\\cernbox-smb\eos\user\j\jwulff\SWAN_projects\batch_jobs2\outputs\datamatrix_files\tri\ref_h14_refvf998"
# DATA_PATH = "//eosproject-smb/eos/project/s/sy-rf-br/Machines/PS/DatasetsForML/Trisplit" # Generalized path


#loaded_values = []
first_file = True


class Datamatrix_lookup_class_tri():
    """
    Class to handle loading and simulation data and interpolating during training.
    """

    def __init__(self, subfolder=r'\ref_h14_refv998', data_path = DATA_PATH, only35k=True):
        
        # Only 35k is used when training RL models: limits the dataset to only search the evenly spaced
        # simulated data to make sure that the trilinear interpolation works correct. For CNN training,
        # the entire simulated dataset is used (i.e. only35k=False).
        self.filepaths = []
        self.dictionary = defaultdict(lambda: defaultdict(dict)) # key 1 is p14*100, key 2 is p21 * 100, key 3 is v14 * 100, value is filepath
        self.p14_labels = None
        self.p21_labels = None
        self.v14_labels = None
        first_file=True
       
        self.data_path = data_path # + subfolder
        print(f'Lookup class collecting data from {self.data_path}.')
        dict_name = f'{os.path.basename(DATA_PATH)}' 
        if only35k:
            dict_name = dict_name + '_35k'
        print(f' Checking if dictionary on disk... {dict_name}')
        # Check if dictionary has already been compiled. If found, load from disk instead of looking through
        if os.path.exists('./lookup_dictionaries/{}.pkl'.format(dict_name)):
            with open('./lookup_dictionaries/{}.pkl'.format(dict_name), 'rb') as f:
                self.dictionary = pickle.load(f)
                self.p14_labels = self.dictionary['p14_labels']
                self.p21_labels = self.dictionary['p21_labels']
                self.v14_labels = self.dictionary['v14_labels']
                self.step_size = self.p14_labels[1]-self.p14_labels[0]
                self.volt_step_size = self.v14_labels[1]-self.v14_labels[0]
                print('Loaded lookup dictionary from file at ./lookup_dictionaries/{}.pkl'.format(dict_name))
        else:
            #os.makedirs('./lookup_dictionaries/{}.pkl'.format(dict_name))
            print('No dict found, creating new dictionary at ./lookup_dictionaries/{}.pkl...'.format(dict_name)) 
        

            if only35k:
                self.p14_labels = np.linspace(-20,20,41)*100
                self.p21_labels = np.linspace(-20,20,41)*100
                self.v14_labels = np.linspace(0.95,1.05,21)*1000
                for p14 in self.p14_labels:
                    for p21 in self.p21_labels:
                        for v14 in self.v14_labels:
                            file_path = self.data_path + '\p14_{}_p21_{}_v14_{}_datamatrix.npy'.format(int(p14), int(p21), int(round(v14)))
                            if first_file:
                                phases_filepaths = np.array([int(p14), int(p21), int(v14), file_path], dtype=object)# phase 42, phase 84, datamatrix)
                                first_file = False
                            else:    
                                #loaded = np.load(file_path, allow_pickle=True)
                                phases_filepaths = np.vstack((phases_filepaths, np.array([int(p14), int(p21), int(round(v14)), file_path], dtype=object)))
            else:
                for file in os.listdir(self.data_path):
                #print(file)
                    if file.endswith("matrix.npy"):
                        # print path name of selected files
                        file_path = os.path.join(self.data_path, file)
                        self.filepaths.append(file_path)


                        #print(file_path)

                        # Extract phases
                        split = file.split('_')
                        p14 = int(split[1])
                        p21 = int(split[3])
                        v14 = int(split[5])
                        if first_file:
                            phases_filepaths = np.array([p14, p21, v14, file_path], dtype=object)# phase 42, phase 84, datamatrix)
                            first_file = False
                        else:    
                            #loaded = np.load(file_path, allow_pickle=True)
                            phases_filepaths = np.vstack((phases_filepaths, np.array([p14, p21, v14, file_path], dtype=object)))
        
# loaded_values now contain all information needed to construct the matrix
# Format: [[phase_42, phase_84, objective_function values], ... ]

            nbr_samples, cols = np.shape(phases_filepaths)
            uniq_p14 = sorted(set(phases_filepaths[:,0]))
            uniq_p21 = sorted(set(phases_filepaths[:,1]))
            uniq_v14 = sorted(set(phases_filepaths[:,2]))

            #data_dims = np.shape(np.load(phases_filepaths[0,2], allow_pickle=True))

            self.p84_dictionary = {}
            # row = p42, col = p84, value is filepath to datamatrix
            self.step_size = uniq_p14[1]-uniq_p14[0]
            self.volt_step_size = uniq_v14[1]-uniq_v14[0]
            self.p14_labels = uniq_p14
            self.p21_labels = uniq_p21
            self.v14_labels = uniq_v14
            for i, p1 in enumerate(uniq_p14):
                for j, p2 in enumerate(uniq_p21):
                    for k, v14 in enumerate(uniq_v14):
                        filepath_idx = np.where((phases_filepaths[:,0] == p1) & (phases_filepaths[:,1] == p2) & (phases_filepaths[:,2] == v14))
                        filepath = phases_filepaths[filepath_idx,3]
                        self.dictionary['{}'.format(p1)]['{}'.format(p2)]['{}'.format(v14)] = filepath
                        #v14_dict['{}'.format(v14)] = '{}'.format(v14): filepath}} 
                    #self.dictionary['{}'.format(p1)] =

            self.dictionary['p14_labels'] = uniq_p14
            self.dictionary['p21_labels'] = uniq_p21
            self.dictionary['v14_labels'] = uniq_v14
            with open('./lookup_dictionaries/{}.pkl'.format(dict_name), 'wb') as f:
                pickle.dump(dict(self.dictionary), f)
            
        print('Loaded lookup dictionary...')
        #matrix = matrix/np.amax(matrix) # normalize
        #np.save('lookup_table_bunch_lengths_quad', matrix)

    def get_datamatrix(self, p14, p21, v14):
        p14, p21, v14 = int(p14*100), int(p21*100), int(v14*1000)
        path = self.dictionary["{}".format(p14)]["{}".format(p21)]['{}'.format(v14)][0][0]
        return np.load(path)

    def get_profile(self, p14, p21, v14):
        p14, p21, v14 = int(p14*100), int(p21*100), int(v14*1000)
        path = self.dictionary["{}".format(p14)]["{}".format(p21)]['{}'.format(v14)][0][0]
        dm = np.load(path)
        profile = dm[-1,:]
        return profile
        #filepath = self.matrix[]

    def get_interpolated_matrix(self, x, y, z, trim=False):  # Insert any continuous p14,p21,v14 and get an interpolated matrix
        # Find closest value of x and y in p42/p84 labels
        x = int(x*100)
        y = int(y*100)
        z = int(z*1000)
        abs_diff_function1 = lambda list_value: abs(list_value - x)
        abs_diff_function2 = lambda list_value: abs(list_value - y)
        abs_diff_function3 = lambda list_value: abs(list_value - z)
        closest_p42 = min(self.p14_labels, key=abs_diff_function1)
        closest_p84 = min(self.p21_labels, key=abs_diff_function2)
        closest_v14 = min(self.v14_labels, key=abs_diff_function3)
        if x>=2000:
            x1 = closest_p42
            x0 = closest_p42
        elif x<=-2000:
            x1 = closest_p42
            x0 = closest_p42
        elif closest_p42 > x:
            x1 = closest_p42
            x0 = (closest_p42-self.step_size)
        elif closest_p42 <= x:
            x1 = (closest_p42+self.step_size)
            x0 = closest_p42
        if y>=2000:
            y1 = closest_p84
            y0 = closest_p84
        elif y<=-2000:
            y1 = closest_p84
            y0 = closest_p84
        elif closest_p84 > y:
            y1 = closest_p84
            y0 = (closest_p84-self.step_size)
        elif closest_p84 <= y:
            y1 = (closest_p84+self.step_size)
            y0 = closest_p84
        if z>=1050:
            z1 = closest_v14
            z0 = closest_v14
        elif z<=950:
            z1 = closest_v14
            z0 = closest_v14
        elif closest_v14 > z:
            z1 = closest_v14
            z0 = (closest_v14-self.volt_step_size)
        elif closest_v14 <= z:
            z1 = (closest_v14+self.volt_step_size)
            z0 = closest_v14
        
        
        
        if x1==x0:
            xd =0
        else:
            xd = (x/100-x0/100)/(x1/100-x0/100)
        if y1==y0:
            yd =0
        else:
            yd = (y/100-y0/100)/(y1/100-y0/100)
        if z1==z0:
            zd=0
        else:
            zd = (z/1000-z0/1000)/(z1/1000-z0/1000)

        # if closest_p42 > x:
        #     x1 = closest_p42
        #     x0 = (closest_p42-self.step_size)
        # if closest_p42 <= x:
        #     x1 = (closest_p42+self.step_size)
        #     x0 = closest_p42
        # if closest_p84 > y:
        #     y1 = closest_p84
        #     y0 = (closest_p84-self.step_size)
        # if closest_p84 <= y:
        #     y1 = (closest_p84+self.step_size)
        #     y0 = closest_p84
        # if closest_v14 > z:
        #     z1 = closest_v14
        #     z0 = (closest_v14-self.volt_step_size)
        # if closest_v14 <= z:
        #     z1 = (closest_v14+self.volt_step_size)
        #     z0 = closest_v14

        # xd = (x/100-x0/100)/(x1/100-x0/100)
        # yd = (y/100-y0/100)/(y1/100-y0/100)
        # zd = (z/1000-z0/1000)/(z1/1000-z0/1000)

        # Load nearest 8 points for trilinear interpolation
        failed_tries=0
        try_to_load=True
        while failed_tries < 5 and try_to_load:
            try:
                c000 = np.load(self.dictionary[str(x0)][str(y0)][str(z0)][0][0])
                c001 = np.load(self.dictionary[str(x0)][str(y0)][str(z1)][0][0])
                c010 = np.load(self.dictionary[str(x0)][str(y1)][str(z0)][0][0])
                c100 = np.load(self.dictionary[str(x1)][str(y0)][str(z0)][0][0])
                c110 = np.load(self.dictionary[str(x1)][str(y1)][str(z0)][0][0])
                c011 = np.load(self.dictionary[str(x0)][str(y1)][str(z1)][0][0])
                c101 = np.load(self.dictionary[str(x1)][str(y0)][str(z1)][0][0])
                c111 = np.load(self.dictionary[str(x1)][str(y1)][str(z1)][0][0])
                try_to_load = False
            except FileNotFoundError as e:
                print("{e} \n File not found, may be due to loss of connection to EOS. Trying again in 30s...")
                time.sleep(30)
                failed_tries += 1
        if failed_tries == 5:
            print("Failed to load datapoint five times, exiting...")
            sys.exit()

        # Interpolate along x

        c00 = c000*(1-xd) + c100*xd
        c01 = c001*(1-xd) + c101*xd
        c10 = c010*(1-xd) + c110*xd
        c11 = c011*(1-xd) + c111*xd

        # Interpolate along y

        c0 = c00*(1-yd) + c10*yd
        c1 = c01*(1-yd) + c11*yd

        # Interpolate along z
        c = c0*(1-zd) + c1*zd
        if trim:
            c=c[:,40:360]
        return c
    
    def get_interpolated_profile(self, x, y, z, trim=False):  # Insert any continuous p14,p21,v14 and get an interpolated matrix
        # Find closest value of x and y in p42/p84 labels
        x = int(x*100)
        y = int(y*100)
        z = int(z*1000)
        abs_diff_function1 = lambda list_value: abs(list_value - x)
        abs_diff_function2 = lambda list_value: abs(list_value - y)
        abs_diff_function3 = lambda list_value: abs(list_value - z)
        closest_p42 = min(self.p14_labels, key=abs_diff_function1)
        if x>=2000:
            x1 = closest_p42
            x0 = closest_p42
        elif x<=-2000:
            x1 = closest_p42
            x0 = closest_p42
        elif closest_p42 > x:
            x1 = closest_p42
            x0 = (closest_p42-self.step_size)
        elif closest_p42 <= x:
            x1 = (closest_p42+self.step_size)
            x0 = closest_p42
        closest_p84 = min(self.p21_labels, key=abs_diff_function2)
        if y>=2000:
            y1 = closest_p84
            y0 = closest_p84
        elif y<=-2000:
            y1 = closest_p84
            y0 = closest_p84
        elif closest_p84 > y:
            y1 = closest_p84
            y0 = (closest_p84-self.step_size)
        elif closest_p84 <= y:
            y1 = (closest_p84+self.step_size)
            y0 = closest_p84
        closest_v14 = min(self.v14_labels, key=abs_diff_function3)
        if z >= 1100:
            z1 = closest_v14
            z0 = closest_v14
        elif z<=900:
            z1 = closest_v14
            z0 = closest_v14
        elif closest_v14 > z:
            z1 = closest_v14
            z0 = (closest_v14-self.volt_step_size)
        elif closest_v14 <= z:
            z1 = (closest_v14+self.volt_step_size)
            z0 = closest_v14
        
        if x1==x0:
            xd =0
        else:
            xd = (x/100-x0/100)/(x1/100-x0/100)
        if y1==y0:
            yd =0
        else:
            yd = (y/100-y0/100)/(y1/100-y0/100)
        if z1==z0:
            zd =0
        else:
            zd = (z/1000-z0/1000)/(z1/1000-z0/1000)

        # Load nearest 8 points for trilinear interpolation
        c000 = self.get_profile(x0/100,y0/100,z0/1000)
        c001 = self.get_profile(x0/100,y0/100,z1/1000)
        c010 = self.get_profile(x0/100,y1/100,z0/1000)
        c100 = self.get_profile(x1/100,y0/100,z0/1000)
        c110 = self.get_profile(x1/100,y1/100,z0/1000)
        c011 = self.get_profile(x0/100,y1/100,z1/1000)
        c101 = self.get_profile(x1/100,y0/100,z1/1000)
        c111 = self.get_profile(x1/100,y1/100,z1/1000)        

        # Interpolate along x

        c00 = c000*(1-xd) + c100*xd
        c01 = c001*(1-xd) + c101*xd
        c10 = c010*(1-xd) + c110*xd
        c11 = c011*(1-xd) + c111*xd

        # Interpolate along y

        c0 = c00*(1-yd) + c10*yd
        c1 = c01*(1-yd) + c11*yd

        # Interpolate along z
        c = c0*(1-zd) + c1*zd
        if trim:
            c = c[40:360]
        return c
        
    def get_available_p14(self):
        return self.p14_labels

    def get_available_p21(self):
        return self.p21_labels
def compute_2d_distance(x1,y1, x2,y2):
    dist = np.sqrt((x2-x1)**2 + (y2-y1)**2)
    return dist

def deg2idx(p42,p84, lookup_class):
    p42max = max(lookup_class.p42_labels)
    p42min = min(lookup_class.p42_labels)
    p84max = max(lookup_class.p84_labels)
    p84min = min(lookup_class.p84_labels)
    nbr_of_p42 = len(lookup_class.p42_labels)
    nbr_of_p84 = len(lookup_class.p84_labels)
    idx = [p42+10,p84+10]
    return idx

def compute_2d_distance(x1,y1, x2,y2):
    dist = np.sqrt((x2-x1)**2 + (y2-y1)**2)
    return dist




# %%
# make plot
# fig, ax = plt.subplots()
  
# # show image
# #shw = ax.imshow(matrix[:,:,3], extent=[min(uniq_p84),max(uniq_p84), max(uniq_p42), min(uniq_p42)])
  
# # make bar
# bar = plt.colorbar(shw)
  
# # show plot with labels
# plt.xlabel('phi_84')
# plt.ylabel('phi_42')
# bar.set_label('ColorBar')
# plt.show()

if __name__ == "__main__":
    DATA_PATH = r"\\eoshome-smb\eos\user\j\jwulff\SWAN_projects\batch_jobs2\outputs\datamatrix_files\tri\ref_h14_refvf998"
    Data = Datamatrix_lookup_class_tri(only35k=True)
    p14_cand = Data.get_available_p14()
    p21_cand = Data.get_available_p21()

    #Test interpolation

    # interpol1 = Data.get_interpolated_matrix(0,0, 0.95)
    # interpol2 = Data.get_interpolated_matrix(0,0, 1.09)
    # interpol3 = Data.get_interpolated_matrix(0,0, 1.0)
    # interpol4 = Data.get_interpolated_matrix(0,0, 0.9)

    save_path =r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\plots\profile_scan"
    p14s = np.linspace(-10,10,11)
    p21s = np.linspace(-10,10,11)
    v14s = np.linspace(0.95,1.05,11)
    plt.ioff()

    for p14 in p14s:
        for p21 in p21s:
            for v14 in v14s:
                dm = Data.get_datamatrix(p14,p21,v14)
                profile = dm[-1,:]
                plt.figure()
                plt.title(f'p14: {p14}, p21: {p21}, v14: {v14}')
                plt.plot(profile)
                plt.savefig(save_path + r'\profile_{}_{}_{}.png'.format(p14,p21,v14))
                plt.close()
    # plt.figure()
    # plt.subplot(221)
    # plt.title('v14: 0.95')
    # plt.imshow(interpol1)
    # plt.subplot(222)
    # plt.title('v14: 1.09')
    # plt.imshow(interpol1)
    # plt.subplot(223)
    # plt.title('v14: 1.0')
    # plt.imshow(interpol1)
    # plt.subplot(224)
    # plt.title('v14: 0.90')
    # plt.imshow(interpol1)
    plt.figure()
    plt.subplot(221)
    plt.title('v14: 0.95')
    plt.imshow(exact1)
    plt.subplot(222)
    plt.title('v14: 1.1')
    plt.imshow(exact2)
    plt.subplot(223)
    plt.title('v14: 1.0')
    plt.imshow(exact3)
    plt.subplot(224)
    plt.title('v14: 0.90')
    plt.imshow(exact4)

    plot_phases = np.array(p14_cand[20:62][::5])
    fig, axs = plt.subplots(9,9, sharex=True, sharey=True)
    for i, p42 in enumerate(plot_phases):
        for j, p84 in enumerate(plot_phases):
            data = Data.get_datamatrix(int(p42/100),int(p84/100))
            axs[i,j].title.set_text("{}_{}".format(int(p42/100),int(p84/100)))
            axs[i,j].imshow(data)
    plt.show()
            
    matrix = Data.get_datamatrix(0,0)
    plt.imshow(matrix)
    print("hej")