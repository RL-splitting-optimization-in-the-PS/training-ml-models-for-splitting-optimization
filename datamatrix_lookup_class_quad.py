# -*- coding: utf-8 -*-
"""
Created on Tue Aug  3 13:44:50 2021

@author: Joel Wulff
"""

# -*- coding: utf-8 -*-

import numpy as np
import os
import matplotlib.pyplot as plt

#SAVE_PATH = r"H:\user\j\jwulff\SWAN_projects\batch_jobs2\outputs\datamatrix_files\quad\assembled_lookup_tables\quad_datamatrix"
DATA_PATH = r"H:\user\j\jwulff\SWAN_projects\batch_jobs2\outputs\datamatrix_files\quad\machine_settings_fixed"
# DATA_PATH = "//eosproject-smb/eos/project/s/sy-rf-br/Machines/PS/DatasetsForML/Quadsplit" # Generalized path



#loaded_values = []
first_file = True


class Datamatrix_lookup_class_quad():

    def __init__(self, data_path=DATA_PATH):
        self.data_path = data_path
        self.filepaths = []
        self.dictionary = None # key 1 is p42*100, key 2 is p84 * 100, value is filepath
        self.p42_labels = None
        self.p84_labels = None
        first_file=True
        for file in os.listdir(self.data_path):
        #print(file)
            if file.endswith("matrix.npy"):
                # print path name of selected files
                file_path = os.path.join(self.data_path, file)
                self.filepaths.append(file_path)


                #print(file_path)

                # Extract phases
                split = file.split('_')
                p42 = int(split[1])
                p84 = int(split[3])
                if first_file:
                    phases_filepaths = np.array([p42, p84, file_path], dtype=object)# phase 42, phase 84, datamatrix)
                    first_file = False
                else:    
                    #loaded = np.load(file_path, allow_pickle=True)
                    phases_filepaths = np.vstack((phases_filepaths, np.array([p42, p84, file_path], dtype=object)))
        
# loaded_values now contain all information needed to construct the matrix
# Format: [[phase_42, phase_84, objective_function values], ... ]

        nbr_samples, cols = np.shape(phases_filepaths)
        uniq_p42 = sorted(set(phases_filepaths[:,0]))
        uniq_p84 = sorted(set(phases_filepaths[:,1]))

        #data_dims = np.shape(np.load(phases_filepaths[0,2], allow_pickle=True))

        self.p84_dictionary = {}
        self.dictionary = {} # row = p42, col = p84, value is filepath to datamatrix
        self.step_size = uniq_p42[1]-uniq_p42[0]
        self.p42_labels = uniq_p42
        self.p84_labels = uniq_p84
        
        #row_dict = {}
        for i, p1 in enumerate(uniq_p42):
            col_dict = {}
            #self.p42_labels[i] = p1
            p1_idx = np.where(phases_filepaths[:,0]==p1)
            for idx in p1_idx:
                for entry in phases_filepaths[idx,:]:
                    col_dict["{}".format(entry[1])] =  entry[2] # entry[1] = p84, entry[2] = filepath
            self.dictionary["{}".format(p1)] =  col_dict
                    #print(col_dict)
                #print(row_dict)
            # for j, p2 in enumerate(uniq_p84):
            #     self.p84_labels[j] = p2
            #     self.matrix = self.matrix[i,j] = row_dict["{}".format(p1)]["{}".format(p2)]
            
        print('hello')
        #matrix = matrix/np.amax(matrix) # normalize
        #np.save('lookup_table_bunch_lengths_quad', matrix)

    

    def get_profile(self, p42, p84, norm=True):
        p42, p84 = int(p42*100), int(p84*100)
        path = self.dictionary["{}".format(p42)]["{}".format(p84)]
        dir_name = os.path.dirname(path) + r'\profiles'
        file = os.path.basename(path)
        split = file.split('_')
        split[-1] = 'profile.npy'
        new_name = '_'.join(split)
        new_path = dir_name + r'\{}'.format(new_name)
        profile = np.load(new_path)
        if norm:
            profile[1] = profile[1]/np.max(profile[1])
        return profile[1] # For now only return values, not time

    def get_interpolated_profile(self, x, y, norm=True):  # Insert any continuous p42,p84 and get an interpolated matrix
        # Find closest value of x and y in p42/p84 labels
        x = int(x*100)
        y = int(y*100)
        abs_diff_function1 = lambda list_value: abs(list_value - x)
        abs_diff_function2 = lambda list_value: abs(list_value - y)
        closest_p42 = min(self.p42_labels, key=abs_diff_function1)
        if x>=3000:
            x1 = closest_p42
            x0 = closest_p42
        elif x<=-3000:
            x1 = closest_p42
            x0 = closest_p42
        elif closest_p42 > x:
            x1 = closest_p42
            x0 = (closest_p42-self.step_size)
        elif closest_p42 <= x:
            x1 = (closest_p42+self.step_size)
            x0 = closest_p42
        closest_p84 = min(self.p84_labels, key=abs_diff_function2)
        if y>=3000:
            y1 = closest_p84
            y0 = closest_p84
        elif y<=-3000:
            y1 = closest_p84
            y0 = closest_p84
        elif closest_p84 > y:
            y1 = closest_p84
            y0 = (closest_p84-self.step_size)
        elif closest_p84 <= y:
            y1 = (closest_p84+self.step_size)
            y0 = closest_p84

        if x1==x0:
            xd =0
        else:
            xd = (x/100-x0/100)/(x1/100-x0/100)
        if y1==y0:
            yd =0
        else:
            yd = (y/100-y0/100)/(y1/100-y0/100)

        q11 = np.load(convert_dm_path_to_profile(self.dictionary[str(x1)][str(y1)]))
        q01 = np.load(convert_dm_path_to_profile(self.dictionary[str(x0)][str(y1)]))
        q00 = np.load(convert_dm_path_to_profile(self.dictionary[str(x0)][str(y0)]))
        q10 = np.load(convert_dm_path_to_profile(self.dictionary[str(x1)][str(y0)]))
        if norm:
            q11 = q11[1]/np.max(q11[1])
            q01 = q01[1]/np.max(q01[1])
            q00 = q00[1]/np.max(q00[1])
            q10 = q10[1]/np.max(q10[1])

        
        # Bilinear interpolation

        # x-direction

        q0 = q00*(1-xd) + q10*xd
        q1 = q01*(1-xd) + q11*xd

        # y-direction

        q = q0*(1-yd) + q1*yd

        

        # dist1 = compute_2d_distance(upper_p42,upper_p84, x,y)
        # dist2 = compute_2d_distance(lower_p42,upper_p84, x,y)
        # dist3 = compute_2d_distance(lower_p42,lower_p84, x,y)
        # dist4 = compute_2d_distance(upper_p42,lower_p84, x,y)
        # dist1234 = [dist1, dist2, dist3, dist4]/(dist1+dist2+dist3+dist4)
        # #print("Distances: {}".format(dist1234))
        # interp_values = (values1*(1-dist1234[0]) + q01*(1-dist1234[1]) + q00*(1-dist1234[2]) + q10*(1-dist1234[3]))

        return q


    def get_datamatrix(self, p42, p84):
        p42, p84 = p42*100, p84*100
        path = self.dictionary["{}".format(p42)]["{}".format(p84)]
        return np.load(path)
        #filepath = self.matrix[]

    def get_interpolated_matrix(self, x, y):  # Insert any continuous p42,p84 and get an interpolated matrix
        # Find closest value of x and y in p42/p84 labels
        x = int(x*100)
        y = int(y*100)
        abs_diff_function1 = lambda list_value: abs(list_value - x)
        abs_diff_function2 = lambda list_value: abs(list_value - y)
        closest_p42 = min(self.p42_labels, key=abs_diff_function1)
        
        if closest_p42 > x:
            upper_p42 = closest_p42
            lower_p42 = closest_p42-self.step_size
        if closest_p42 <= x:
            upper_p42 = closest_p42+self.step_size
            lower_p42 = closest_p42
        closest_p84 = min(self.p84_labels, key=abs_diff_function2)
        if closest_p84 > y:
            upper_p84 = closest_p84
            lower_p84 = closest_p84-self.step_size
        if closest_p84 <= y:
            upper_p84 = closest_p84+self.step_size
            lower_p84 = closest_p84




        values1 = np.load(self.dictionary[str(upper_p42)][str(upper_p84)])
        q01 = np.load(self.dictionary[str(lower_p42)][str(upper_p84)])
        q00 = np.load(self.dictionary[str(lower_p42)][str(lower_p84)])
        q10 = np.load(self.dictionary[str(upper_p42)][str(lower_p84)])

        # Compute distances

        dist1 = compute_2d_distance(upper_p42,upper_p84, x,y)
        dist2 = compute_2d_distance(lower_p42,upper_p84, x,y)
        dist3 = compute_2d_distance(lower_p42,lower_p84, x,y)
        dist4 = compute_2d_distance(upper_p42,lower_p84, x,y)
        dist1234 = [dist1, dist2, dist3, dist4]/(dist1+dist2+dist3+dist4)
        #print("Distances: {}".format(dist1234))
        interp_values = (values1*(1-dist1234[0]) + q01*(1-dist1234[1]) + q00*(1-dist1234[2]) + q10*(1-dist1234[3]))

        return interp_values
        
    def get_available_p42(self):
        return self.p42_labels

    def get_available_p84(self):
        return self.p84_labels

def convert_dm_path_to_profile(path):
    dir_name = os.path.dirname(path) + r'\profiles'
    file = os.path.basename(path)
    split = file.split('_')
    split[-1] = 'profile.npy'
    new_name = '_'.join(split)
    new_path = dir_name + r'\{}'.format(new_name)
    return new_path

def compute_2d_distance(x1,y1, x2,y2):
    dist = np.sqrt((x2-x1)**2 + (y2-y1)**2)
    return dist

def deg2idx(p42,p84, lookup_class):
    p42max = max(lookup_class.p42_labels)
    p42min = min(lookup_class.p42_labels)
    p84max = max(lookup_class.p84_labels)
    p84min = min(lookup_class.p84_labels)
    nbr_of_p42 = len(lookup_class.p42_labels)
    nbr_of_p84 = len(lookup_class.p84_labels)
    idx = [p42+10,p84+10]
    return idx

def compute_2d_distance(x1,y1, x2,y2):
    dist = np.sqrt((x2-x1)**2 + (y2-y1)**2)
    return dist




# %%
# make plot
# fig, ax = plt.subplots()
  
# # show image
# #shw = ax.imshow(matrix[:,:,3], extent=[min(uniq_p84),max(uniq_p84), max(uniq_p42), min(uniq_p42)])
  
# # make bar
# bar = plt.colorbar(shw)
  
# # show plot with labels
# plt.xlabel('phi_84')
# plt.ylabel('phi_42')
# bar.set_label('ColorBar')
# plt.show()

if __name__ == "__main__":
    Data = Datamatrix_lookup_class_quad()
    Data.get_interpolated_profile(0,0.25)

    #Test interpolation

    interpol1 = Data.get_interpolated_matrix(11.2,13.7)
    interpol2 = Data.get_interpolated_matrix(1.2,-0.5)
    interpol3 = Data.get_interpolated_matrix(-2.1,1.2)
    interpol4 = Data.get_interpolated_matrix(4,19.2)

    plt.figure()
    plt.subplot(141)
    plt.imshow(interpol1)
    plt.subplot(142)
    plt.imshow(interpol1)
    plt.subplot(143)
    plt.imshow(interpol1)
    plt.subplot(144)
    plt.imshow(interpol1)

    plot_phases = np.array(p42_cand[20:62][::5])
    fig, axs = plt.subplots(9,9, sharex=True, sharey=True)
    for i, p42 in enumerate(plot_phases):
        for j, p84 in enumerate(plot_phases):
            data = Data.get_datamatrix(int(p42/100),int(p84/100))
            axs[i,j].title.set_text("{}_{}".format(int(p42/100),int(p84/100)))
            axs[i,j].imshow(data)
    plt.show()
            
    matrix = Data.get_datamatrix(0,0)
    plt.imshow(matrix)
    print("hej")