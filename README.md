# Purpose

A repository to store all necessary code for training and designing new RL or CNN models to optimize the triple or quadruple splittings in the PS. Based on the data simulated using https://gitlab.cern.ch/RL-splitting-optimization-in-the-PS/simulating-data-for-quad-and-triple-splittings.

Currently, all data used to train these types of models has been store on the personal EOS space of Joel Wulff, and as such many paths in the training scripts will need to be adjusted to point to the dataset of your choice before starting training.


# Train CNN models

To try to train a CNN model to be used for the splitting optimization, see the ```train_model.py``` script. The current settings will attempt to train a CNN model on the triple splitting dataset, but the options in the hparams variable can be changed along with the dataset to use to train other models.
