from unicodedata import name
import torch
import torch.nn as nn
import torch.nn.functional as F
#from pytorch_fitmodule import FitModule
from torch.autograd import Variable
import numpy as np
#from utils import count_parameters
#import torchviz
#from utils import count_parameters
#from neural_networks.dataloader import SplittingDataset, ToTensor, Normalize
#from dataloader import SplittingDataset, ToTensor, Normalize

from torchvision import transforms#, utils
from torch.utils.data import Dataset, DataLoader
import os
os.environ["PATH"] += os.pathsep + 'C:/Program Files/Graphviz/bin/'


def linear_layers(input_feats, num_features):
    dense_layers = nn.Sequential(
            torch.nn.Linear(input_feats,256), # When using original sim sizes
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Linear(256, num_features)
        )
    return dense_layers

class CNN_tri_new(torch.nn.Module):
    """
    CNN for use with triple splitting tomoscope-like data. Used with trim_edges=True for
    operational scripts.
    """
    def __init__(self, num_features=3, trim_edges=False):
        super(CNN_tri_new, self).__init__()
        

        self.conv_layers = nn.Sequential(# Input is 1x150x400 
            torch.nn.Conv2d(1,64,3,stride=2), # 
            torch.nn.BatchNorm2d(64),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Conv2d(64,32,3,2), # 
            torch.nn.BatchNorm2d(32),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Conv2d(32,32,3,2), # 
            torch.nn.BatchNorm2d(32),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Conv2d(32,32,3,2), # 
            torch.nn.BatchNorm2d(32),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Conv2d(32,32,3,2), # 
            torch.nn.BatchNorm2d(32),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Flatten(),
            
        )
        if trim_edges:
            flattened_feats = 864
        else:
            flattened_feats = 1056
        self.dense_layers = linear_layers(flattened_feats, num_features)

    def forward(self, x):                                        
        x = x.float()
        
        x = self.conv_layers(x)
        out = self.dense_layers(x)
        return out

class CNN_tri_avg_pool(torch.nn.Module):
    def __init__(self, num_features=3, trim_edges=False):
        super(CNN_tri_avg_pool, self).__init__()
        

        self.conv_layers = nn.Sequential(# Input is 1x150x400
            torch.nn.AvgPool2d(kernel_size=(2,4),stride=2),
            torch.nn.Conv2d(1,64,3,stride=2), #
            torch.nn.BatchNorm2d(64),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Conv2d(64,32,3,2), # 
            torch.nn.BatchNorm2d(32),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Conv2d(32,32,3,2), # 
            torch.nn.BatchNorm2d(32),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Conv2d(32,32,3,2), # 
            torch.nn.BatchNorm2d(32),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Flatten(),
            
        )
        if trim_edges:
            flattened_feats = 864
        else:
            flattened_feats = 1056
        self.dense_layers = linear_layers(flattened_feats, num_features)

    def forward(self, x):                                        
        x = x.float()
        
        x = self.conv_layers(x)
        out = self.dense_layers(x)
        return out

if __name__ == "__main__":
    cnn = CNN_tri_avg_pool(trim_edges=True)
    nbr_params = count_parameters(cnn)
    make_graph = True
    if make_graph:
        dataset = SplittingDataset(noise=True, trim_edges=True, csv_file = r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\dataset\tri\ref_voltage\dataset_tri_59521_ref.csv",
        transform=transforms.Compose([
            Normalize(),
            ToTensor(),
        ]))
        dataloader = DataLoader(dataset, batch_size=5,  shuffle=False, num_workers=0)
        data = next(iter(dataloader))
        inputs, labels = data['image'].float(), data['labels'].float()
        test = torch.nn.AvgPool2d(kernel_size=(2,4),stride=2)
        inp = test(inputs) # Check effect of avg pooling: Smoothing
        outputs = cnn(inputs)
        #torchviz.make_dot(outputs, params=dict(list(cnn.named_parameters()))).render("cnn_torchviz", format="png")
    print(cnn)
    print(nbr_params)