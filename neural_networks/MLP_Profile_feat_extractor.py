import torch
import torch.nn as nn
import torch.nn.functional as F
#from pytorch_fitmodule import FitModule
from torch.autograd import Variable
from torchvision import transforms, utils
from torch.utils.data import Dataset, DataLoader
import torch.optim as optim

# profile input: [1,200] numpy array of values, normalized to the range [0,1]

class MLPProfile(torch.nn.Module):
    def __init__(self, input_features = 200, output_features=2):
        super(MLPProfile, self).__init__()

        self.network = nn.Sequential(# 
            torch.nn.Linear(200, 100),
            torch.nn.ReLU(),
            torch.nn.Linear(100, output_features),
            #torch.nn.ReLU(),
            #torch.nn.Linear(64,output_features),
            torch.nn.Tanh()
        )


    def forward(self, x):    # Define forward pass.                                      
        x = x.float()                                              
        out = self.network(x)
        return out