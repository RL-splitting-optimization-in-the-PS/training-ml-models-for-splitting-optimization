
import torch
import torch.nn as nn
import torch.nn.functional as F
#from pytorch_fitmodule import FitModule
from torch.autograd import Variable
import numpy as np
#from utils import count_parameters
#import torchviz
#from utils import count_parameters
from dataloader import SplittingDataset, ToTensor, Normalize
from torchvision import transforms, utils
from torch.utils.data import Dataset, DataLoader
import os
os.environ["PATH"] += os.pathsep + 'C:/Program Files/Graphviz/bin/'

def linear_layers(input_layers, num_features):
    dense_layers = nn.Sequential(
            torch.nn.Linear(input_layers,256),
            torch.nn.ReLU(),
            torch.nn.Linear(256, num_features)
        )
    return dense_layers


class CNN(torch.nn.Module):
    """
    Class defining CNN structure for use with quadruple splitting tomoscope-like data. Not currently used for
    operational scripts.
    """

    def __init__(self, num_features=2, trim_edges=False):
        super(CNN, self).__init__()
        self.trim_edges=trim_edges
        self.encoder = nn.Sequential(#
            torch.nn.Conv2d(1,64,3,stride=1), # 150x200
            torch.nn.BatchNorm2d(64),
            torch.nn.ReLU(),
            torch.nn.Conv2d(64,32,3,2), # 150x200x64
            torch.nn.BatchNorm2d(32),
            torch.nn.ReLU(),
            torch.nn.Conv2d(32,32,3,2), # 75 x 100x32
            torch.nn.BatchNorm2d(32),
            torch.nn.ReLU(),
            torch.nn.Conv2d(32,32,3,2), # 37 x 50 (32,64,32)
            torch.nn.BatchNorm2d(32),
            torch.nn.ReLU(),
            torch.nn.Conv2d(32,32,3,2), # 316 x 500 (32,64,32)
            torch.nn.BatchNorm2d(32),
            torch.nn.ReLU(),
            torch.nn.Conv2d(32,32,3,2), # 316 x 500 (32,64,32)
            torch.nn.BatchNorm2d(32),
            torch.nn.ReLU(),
            torch.nn.Flatten(),
            torch.nn.Linear(480,256),
            torch.nn.ReLU(),
            torch.nn.Linear(256, num_features)
        )
        #self.dense = linear_layers(480, num_features)

    def forward(self, x):                                        
        x = x.float()
        
        x = self.encoder(x)
        #out = self.dense_layers(x)
        return x #out

class CNN_alt(torch.nn.Module):
    def __init__(self, num_features=2):
        super(CNN, self).__init__()

        self.encoder = nn.Sequential(# Input is 632x1000x1 (try rescale to 512x1024?) 
            torch.nn.Conv2d(1,16,9,stride=2), # 316 x 500 (256,512)
            torch.nn.BatchNorm2d(16),
            torch.nn.LeakyReLU(0.1),
            torch.nn.Conv2d(16,32,3,stride=2),
            torch.nn.BatchNorm2d(32),
            torch.nn.LeakyReLU(0.1),
            torch.nn.Flatten(),
            torch.nn.Linear(1792,512),
            torch.nn.LeakyReLU(0.1),
            torch.nn.Linear(512, num_features)
        )

    def _make_layer(self, block, planes, num_blocks, stride):
        strides = [stride] + [1] * (num_blocks - 1)
        layers = []
        for stride in strides:
            layers.append(block(self.in_planes, planes, stride))
            self.in_planes = planes * block.expansion
        return nn.Sequential(*layers)

    def forward(self, x):    # add additional layers here?                                       
        x = x.float()                                              
        out = self.encoder(x)
        return out

if __name__ == "__main__":
    cnn = CNN()
    #nbr_params = count_parameters(cnn)
    make_graph = False
    if make_graph:
        dataset = SplittingDataset(root_dir =r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\dataset",
        transform=transforms.Compose([
            Normalize(),
            ToTensor(),
        ]))
        dataloader = DataLoader(dataset, batch_size=5, shuffle=False, num_workers=0)
        data = next(iter(dataloader))
        inputs, labels = data['image'].float(), data['labels'].float()
        outputs = cnn(inputs)
#        torchviz.make_dot(outputs, params=dict(list(cnn.named_parameters()))).render("cnn_torchviz", format="png")
    print(cnn)
    #print(nbr_params)