import sys
sys.path.append(r'/eos/home-j/jwulff/workspaces/RL-007-RFinPS/optimising-rf-manipulations-in-the-ps-through-rl/optimising-rf-manipulations-in-the-ps-through-rl')
import torch
import torch.nn as nn
import torch.nn.functional as F
#from pytorch_fitmodule import FitModule
from torch.autograd import Variable
import numpy as np
from dataloader import QuadsplitDataset, ToTensor, Normalize, ResizeTri
from torchvision import transforms, utils
from torch.utils.data import Dataset, DataLoader
from CNN import CNN
from CNN_tri import CNN_tri
from ResNet_model import BasicBlock
import torch.optim as optim
from utils import count_parameters
import matplotlib.pyplot as plt

problem = 'tri'

batch_size = 20
dataset = QuadsplitDataset(csv_file =r"/eos/home-j/jwulff/workspaces/RL-007-RFinPS/optimising-rf-manipulations-in-the-ps-through-rl/dataset/tri/dataset_tri.csv",
    transform=transforms.Compose([
        Normalize(),
        ResizeTri(),
        ToTensor(),
    ]),
    vpc=True)
length = dataset.__len__()
#small = int(length*0.1)
#big = length - small
#small_dataset, _ = torch.utils.data.random_split(dataset, [small, big], generator=torch.Generator().manual_seed(42))
#length = small_dataset.__len__()
train_len = int(length * 0.9)
 
val_len = length - train_len
train, val = torch.utils.data.random_split(dataset, [train_len, val_len], generator=torch.Generator().manual_seed(42))
train_dataloader = DataLoader(train, batch_size=batch_size, shuffle=True, num_workers=0)
val_dataloader = DataLoader(val, batch_size=batch_size, shuffle=True, num_workers=0)

model = CNN_tri()
optimizer = optim.Adam(model.parameters(), lr=1e-3)

model_name = 'cnn_tri_2lin_Resized_inputsMar_29_2022_10-37-38_e5_continue__e11'
print(f'Fetching model {model_name}...')
path = r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\saved_models\{}.pth".format(model_name)
path = r"/eos/home-j/jwulff/workspaces/RL-007-RFinPS/optimising-rf-manipulations-in-the-ps-through-rl/saved_models/{}.pth".format(model_name) #VPC
checkpoint = torch.load(path)
model.load_state_dict(checkpoint['model_state_dict'])
optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
criterion = checkpoint['loss']
print(f'Weights loaded...')

model.eval()
preds=[]
true=[]
results = torch.empty(0)
for i, data in enumerate(val_dataloader, 0):
    with torch.no_grad():
        inputs, labels = data['image'].float(), data['labels'].float()
        outputs = model(inputs)
        loss = criterion(outputs,labels)
        preds.append(outputs)
        true.append(labels)
        preds_labels = torch.cat((outputs, labels), 1)
        results = torch.cat((results, preds_labels),0)
        #if i % 3:
        #    break
        #eval_loss += loss.item()
        #avg_loss += loss.item()
        #print("Preds {}, True {}, Loss {}".format(outputs/100,labels/100,loss))
        #print("[Preds, True, {}]".format(preds_labels/100))

# Make some plots
print(results)

if problem=='quad':
    p42_out_array = results[:,0]/100
    p42_in_array = results[:,3]/100

    p84_out_array = results[:,1]/100
    p84_in_array = results[:,4]/100

    plt.figure()
    x=np.linspace(-25,25,100)
    plt.plot(x,x,'g')
    plt.plot(p42_in_array, p42_out_array, '.')
    plt.legend(['Reference', 'in vs out'])
    plt.xlabel('p42 phase input')
    plt.ylabel('p42 phase output')
    plt.savefig('plots/{}_p42.png'.format(model_name))

    plt.figure()
    plt.plot(x,x,'g')
    plt.plot(p84_in_array, p84_out_array, '.')
    plt.legend(['Reference', 'in vs out'])
    plt.xlabel('p84 phase input')
    plt.ylabel('p84 phase output')
    plt.savefig('plots/{}_p84.png'.format(model_name))

    plt.figure()
    zeros = np.zeros(len(x))
    x=np.linspace(-25,25,100)
    plt.plot(x,zeros,'g')
    plt.plot(p42_in_array, p42_out_array-p42_in_array, '.')
    plt.legend(['Reference', 'in vs out'])
    plt.xlabel('p42 phase input')
    plt.ylabel('p42 output - p42 input')
    plt.savefig('plots/{}_p42err.png'.format(model_name))

    plt.figure()

    plt.plot(x,zeros,'g')
    plt.plot(p84_in_array, p84_out_array-p84_in_array, '.')
    plt.legend(['Reference', 'in vs out'])
    plt.xlabel('p84 phase input')
    plt.ylabel('p84 output - p84 input')
    plt.savefig('plots/{}_p84err.png'.format(model_name))
    plt.show()

    # Save plots
if problem=='tri':
    p14_out_array = results[:,0]/100
    p14_in_array = results[:,3]/100

    p21_out_array = results[:,1]/100
    p21_in_array = results[:,4]/100

    v14_out_array = results[:,2]/1000
    v14_in_array = results[:,5]/1000

    plt.figure()
    x=np.linspace(-15,15,100)
    plt.plot(x,x,'g')
    plt.plot(p14_in_array, p14_out_array, '.')
    plt.legend(['Reference', 'in vs out'])
    plt.xlabel('p14 phase input')
    plt.ylabel('p14 phase output')
    plt.savefig('plots/{}_p14.png'.format(model_name))

    plt.figure()
    plt.plot(x,x,'g')
    plt.plot(p21_in_array, p21_out_array, '.')
    plt.legend(['Reference', 'in vs out'])
    plt.xlabel('p21 phase input')
    plt.ylabel('p21 phase output')
    plt.savefig('plots/{}_p21.png'.format(model_name))

    plt.figure()
    x_v14 = np.linspace(0.85,1.15,100)
    plt.plot(x_v14,x_v14,'g')
    plt.plot(v14_in_array, v14_out_array, '.')
    plt.legend(['Reference', 'in vs out'])
    plt.xlabel('v14 input')
    plt.ylabel('v14 output')
    plt.savefig('plots/{}_v14.png'.format(model_name))

    plt.figure()
    zeros = np.zeros(len(x))
    x=np.linspace(-15,15,100)
    plt.plot(x,zeros,'g')
    plt.plot(p14_in_array, p14_out_array-p14_in_array, '.')
    plt.legend(['Reference', 'in vs out'])
    plt.xlabel('p14 phase input')
    plt.ylabel('p14 output - p14 input')
    plt.savefig('plots/{}_p14err.png'.format(model_name))

    plt.figure()

    plt.plot(x,zeros,'g')
    plt.plot(p21_in_array, p21_out_array-p21_in_array, '.')
    plt.legend(['Reference', 'in vs out'])
    plt.xlabel('p21 phase input')
    plt.ylabel('p21 output - p21 input')
    plt.savefig('plots/{}_p21err.png'.format(model_name))

    plt.figure()

    plt.plot(x_v14,zeros,'g')
    plt.plot(v14_in_array, v14_out_array-v14_in_array, '.')
    plt.legend(['Reference', 'in vs out'])
    plt.xlabel('v14 phase input')
    plt.ylabel('v14 output - v14 input')
    plt.savefig('plots/{}_v14err.png'.format(model_name))
    plt.show()





print("Inference complete.")