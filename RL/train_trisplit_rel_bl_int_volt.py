#%%
from os import name
import gym
import numpy as np
from tri_env_rel_bl_int_voltage_loss import TriEnvRelBLIntVoltage
from stable_baselines3 import SAC, HerReplayBuffer
from stable_baselines3 import TD3
from stable_baselines3 import DDPG
from stable_baselines3 import her
#from stable_baselines3.common.policies import FeedForwardPolicy
from stable_baselines3.common.noise import NormalActionNoise, OrnsteinUhlenbeckActionNoise
from stable_baselines3.common.vec_env import DummyVecEnv
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.common.results_plotter import load_results, ts2xy
#from stable_baselines3 HerReplayBuffer
import matplotlib.pyplot as plt
from stable_baselines3.common.callbacks import CheckpointCallback
from stable_baselines3.common.callbacks import EvalCallback, StopTrainingOnRewardThreshold


# Save a checkpoint every 1000 steps
# Separate evaluation env
eval_env = TriEnvRelBLIntVoltage(max_step_volt=0.02)
# Use deterministic actions for evaluation
callback_on_best = StopTrainingOnRewardThreshold(reward_threshold=200, verbose=1)
#for reward_scale in range(20):
#    ent_coef = 1/(reward_scale+1)
#callback_on_new_best=callback_on_best,
eval_callback = EvalCallback(eval_env, callback_on_new_best=callback_on_best,  best_model_save_path='./RL_logs/tri/volt/',
    log_path='./logs/', eval_freq=500,
    deterministic=True, render=False)


env = TriEnvRelBLIntVoltage(max_step_volt=0.02)
seed = 7
np.random.seed(seed)
env.seed(seed)
log_dir = './'
#env = Monitor(env, log_dir, allow_early_resets=True)

nb_actions = env.action_space.shape[0]

#env = DummyVecEnv([lambda: env])
# The noise objects for TD3
n_actions = env.action_space.shape[-1]
action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.01 * np.ones(n_actions))
#action_noise = OrnsteinUhlenbeckActionNoise(mean=np.zeros(n_actions), sigma=0.1 * np.ones(n_actions))


#policy_kwargs = dict(net_arch=[64,64])# policy_kwargs=policy_kwargs, buffer_size=5000,
#learning_rate=0.001,
# action_noise=action_noise,
# ent_coef=ent_coef,
ent_coef = 1/5
n_sampled_goal = 4
model = SAC("MlpPolicy", 
        env, 
        ent_coef=ent_coef, 
        verbose=1, 
        tensorboard_log="./trisplit_tensorboard/",
        # replay_buffer_class=HerReplayBuffer,
        # replay_buffer_kwargs=dict(
        #     n_sampled_goal=n_sampled_goal,
        #     goal_selection_strategy="future",
        #     )
    )
print("Starting training...")
model.learn(total_timesteps=50000, callback=eval_callback, log_interval=5, tb_log_name="SAC-RelBLIntVolt-voltloss-step01-profile-59521-ref-NoiseMoveInjPV-diff-00010")
print("Completed training")
#model.save("twosplit")
env = model.get_env()

del model # remove to demonstrate saving and loading

#%%

model = SAC.load("./logs/tri/best_model")

obs = env.reset()
performance = [0,0]
predicted_phase_error_when_done = []
for _ in range(500):
    #print(obs)
    action, _states = model.predict(obs)
    #print(action*10)
    obs, rewards, done, info = env.step(action)
    #print(obs)
    #env.render()
    if done:
        print(f"Took {_} steps before terminating test")
        print(f"Info {info}")
        if info[0]['success'] == True:
            performance[0] += 1
        else:
            performance[1] += 1
        #predicted_phase_error_when_done.append(info[0]['initial_phase'] + info[0]['phase_corr'])
        obs = env.reset()
print(f"Succesful optimizations: {performance[0]}")
print(f"Unsuccesful optimizations: {performance[1]}")
print(f"Accuracy: {performance[0]/(performance[0]+performance[1])}")

p42_err = []
p84_err = []


# %%
