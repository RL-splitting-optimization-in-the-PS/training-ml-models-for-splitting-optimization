#%%
from os import name
import gym
import numpy as np
from tri_env_rel_bl_int import TriEnvRelBLInt
from stable_baselines3 import SAC, HerReplayBuffer
from stable_baselines3 import TD3
from stable_baselines3 import DDPG
from stable_baselines3 import her
#from stable_baselines3.common.policies import FeedForwardPolicy
from stable_baselines3.common.noise import NormalActionNoise, OrnsteinUhlenbeckActionNoise
from stable_baselines3.common.vec_env import DummyVecEnv
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.common.results_plotter import load_results, ts2xy
#from stable_baselines3 HerReplayBuffer
import matplotlib.pyplot as plt
from stable_baselines3.common.callbacks import CheckpointCallback
from stable_baselines3.common.callbacks import EvalCallback, StopTrainingOnRewardThreshold


phase_step_size = 10
step_volt=0.02
# Save a checkpoint every 1000 steps
# Separate evaluation env
eval_env = TriEnvRelBLInt(max_step_size=phase_step_size, max_step_volt=step_volt)
# Use deterministic actions for evaluation
callback_on_best = StopTrainingOnRewardThreshold(reward_threshold=200, verbose=1)
#for reward_scale in range(20):
#    ent_coef = 1/(reward_scale+1)
#callback_on_new_best=callback_on_best,
eval_callback = EvalCallback(eval_env, callback_on_new_best=callback_on_best,  best_model_save_path='./RL_logs/tri/',
    log_path='./logs/', eval_freq=500,
    deterministic=True, render=False)

# Save a checkpoint every 1000 steps
checkpoint_callback = CheckpointCallback(
  save_freq=100000,
  save_path="./RL_logs/tri/SAC",
  name_prefix="best_model_checkpoint",
  save_replay_buffer=True,
)






env = TriEnvRelBLInt(max_step_size=phase_step_size, max_step_volt=step_volt)
seed = 7
np.random.seed(seed)
env.seed(seed)
log_dir = './'
#env = Monitor(env, log_dir, allow_early_resets=True)

nb_actions = env.action_space.shape[0]

#env = DummyVecEnv([lambda: env])
# The noise objects for TD3
n_actions = env.action_space.shape[-1]
action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.01 * np.ones(n_actions))
#action_noise = OrnsteinUhlenbeckActionNoise(mean=np.zeros(n_actions), sigma=0.1 * np.ones(n_actions))


#policy_kwargs = dict(net_arch=[64,64])# policy_kwargs=policy_kwargs, buffer_size=5000,
#learning_rate=0.001,
# action_noise=action_noise,
# ent_coef=ent_coef,

### IF LOADING ###
# path_to_saved_models = r'C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\RL_logs\tri'
# print('Restarting training of existing model...')
# model = SAC.load(path_to_saved_models + r'\SAC_all_params_checkpoint_850000_steps', env=env)
# model.load_replay_buffer(path_to_saved_models + r'\SAC_all_params_checkpoint_replay_buffer_800000_steps')

"# The noise objects for TD3. Injects noise in the actions to encourage exploration.\n",
n_actions = env.action_space.shape[-1]
action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.1 * np.ones(n_actions))

# Define your model

### TD3
# model = TD3("MlpPolicy", 
#             env, 
#             action_noise=action_noise, 
#             verbose=1, 
#             tensorboard_log="./trisplit_BL_tensorboard/",
# )

### SAC
ent_coef = 1/5
n_sampled_goal = 4
model = SAC("MlpPolicy", 
        env, 
        ent_coef=ent_coef, 
        verbose=1, 
        tensorboard_log="./trisplit_tensorboard/",
        # replay_buffer_class=HerReplayBuffer,
        # replay_buffer_kwargs=dict(
        #     n_sampled_goal=n_sampled_goal,
        #     goal_selection_strategy="future",
        #     )
    )
print("Starting training...")
model.learn(total_timesteps=1000000, callback=[eval_callback, checkpoint_callback], log_interval=5, tb_log_name="SAC-RelBLInt-allparams-step10-002-59521-ref-NoiseMoveInj-diff-00008")
print("Completed training")
model.save("Finished_SAC-RelBLInt-allparams-2mil")
env = model.get_env()

del model # remove to demonstrate saving and loading

# %%
