#%%
import gym
import numpy as np
from numpy.core.fromnumeric import mean, shape
from numpy.core.function_base import linspace
from sklearn.metrics import mean_squared_error
#from tri_env_bl_cnn import BUNCH_LENGTH_CRITERIA
#from tri_env_bl_int_cnn import TriEnvBLIntCNN
from tri_env_rel_bl_int_cnn_phase_loss import TriEnvRelBLIntCNNPhase
from tri_env_rel_bl_int_phase_loss import TriEnvRelBLIntPhase
from scipy.optimize import minimize
from scipy.optimize import Bounds
import pybobyqa

from utils import profile_reward_tri, isolate_bunches_from_dm_profile_tri, loss_function_tri, tri_phase_loss
from neural_networks.dataloader import ToTensor, Normalize, AddNoise
from torchvision import transforms

from stable_baselines3 import SAC
from stable_baselines3 import TD3
from stable_baselines3 import DDPG
from stable_baselines3 import her
#from stable_baselines3.common.policies import FeedForwardPolicy
from datamatrix_lookup_class_tri import Datamatrix_lookup_class_tri

from stable_baselines3.common.results_plotter import load_results, ts2xy
#from stable_baselines3 HerReplayBuffer
import matplotlib.pyplot as plt

from vpc_functions import render_voltage

import os




data_class = Datamatrix_lookup_class_tri(only35k=True)

transform=transforms.Compose([
            Normalize(),
            ToTensor(),
            AddNoise(),
        ])

# SAC-BLIntCnn-Simple-profile-59521-ref-NoiseMoveInjPV-diff-0001
# SAC-CNN-only-step001-Simple-profile-VPC-rew-59521-ref-NoiseMoveInjPV-diff-0001
# SAC-RelBLIntCnnPhase-simple-profile-step10-59521-ref-NoiseMoveInjPV-diff-00015
model_name = r"SAC-RelBLIntPhase-phase-loss-profile-step10-59521-ref-NoiseMoveInjPV-diff-00012"
model = SAC.load("./RL_logs/tri/{}".format(model_name))

volt_model_name = r"SAC-RelBLIntVolt-gauss-profile-59521-ref-NoiseMoveInjPV-diff-00009"
volt_model = SAC.load("./RL_logs/tri/{}".format(volt_model_name))

env = TriEnvRelBLIntPhase(max_step_size=5)

obs = env.reset()
performance = [0,0]
steps_per_episode = []
predicted_phase_error_when_done = []
end_bunch_lengths = []
end_se = []
start_diff_estimates = []
end_diff_estimates = []
initial_profiles = []
final_profiles = []
counter = 0
mode='human' # 'console'
render=False
#init plotting window


for episode in range(10):
    done=False
    first_step=True
    volt_step=0
    while not done:
        action, _states = model.predict(obs, deterministic=True)
        #print(action*10)
        obs, rewards, done, info = env.step(action)
        #print(f'Observation: {obs}')
        if render:
            env.render(mode=mode) # Use if you want to observe the agent
        if first_step:
            start_diff_estimates.append(info['diff_estimate'])
            if counter < 5:
                initial_profiles.append(info['profile'])
        first_step=False
        
        if done:
            if render:
                env.render(mode=mode, done=True)
            print(f"Took {info['steps']} steps to optimise phase")
            print(f"final loss: {info['diff_estimate']}")
            print(f"Info {info['success']}, {info['steps']}, {info['initial_phase']}, {info['phase_corr']}")
            steps_per_episode.append(info['steps'])
            end_diff_estimates.append(info['diff_estimate'])
            if info['success'] == True:
                performance[0] += 1
            else:
                performance[1] += 1
            end_bunch_lengths.append(obs[:3])

            bls = np.array(obs[:3])
            target = np.mean(bls)
            end_se.append(np.sum((bls-target)**2)/len(bls))

            predicted_phase_error_when_done.append(info['initial_phase'] + info['phase_corr'])
            if counter < 5:
                final_profiles.append(info['profile'])
            counter += 1


            ### Perform voltage optimization when phase is good!! ###
            # Load state from environment
            phase_volt_set = env.phase_volt_set # [p14,p21,v14]

            def scipy_function(x, phase_volt_set=phase_volt_set):
                volt = x # [p14,p21,v14]

                datamatrix = data_class.get_interpolated_matrix(phase_volt_set[0], phase_volt_set[1], volt[0])
                    # Convert input into normalized tensor
                sample = {}
                sample['image'] = datamatrix
                sample['labels'] = np.array([phase_volt_set[0], phase_volt_set[1], volt[0]])
                transformed_sample = transform(sample)
                input = transformed_sample['image']
                
                criterion = 0.0012
                
                profile = input[0,-1,:].numpy()
                bunches, fwhms, intensities = isolate_bunches_from_dm_profile_tri(profile, intensities=True)
                fwhms = fwhms - np.mean(fwhms)
                intensities = intensities / max(intensities) # Check that 50 is reasonable #TODO
                intensities = intensities - np.mean(intensities)
                observation = np.append(fwhms, intensities)
                
                loss = loss_function_tri(bunches[0], bunches[1], bunches[2])#voltage_loss(profile) # Compute loss
                return loss
            bounds = Bounds([0.95], [1.05])
            #res = minimize(scipy_function, phase_volt_set[2], method='nelder-mead', bounds=bounds, options={'disp': True})
            soln = pybobyqa.solve(scipy_function, np.array([phase_volt_set[2]]), rhobeg=0.05, rhoend=1e-4, bounds=(np.array([0.95]),np.array([1.05])), print_progress=True)
            best_voltage = soln.x[0]
            ####  Get next datapoint ####
            datamatrix = data_class.get_interpolated_matrix(phase_volt_set[0], phase_volt_set[1], best_voltage)
                # Convert input into normalized tensor
            sample = {}
            sample['image'] = datamatrix
            sample['labels'] = np.array([phase_volt_set[0], phase_volt_set[1], phase_volt_set[2]])
            transformed_sample = transform(sample)
            input = transformed_sample['image']
            profile = input[0,-1,:].numpy()
            bunches, fwhms, intensities = isolate_bunches_from_dm_profile_tri(profile, intensities=True)
            fwhms = fwhms - np.mean(fwhms)
            intensities = intensities / max(intensities) # Check that 50 is reasonable #TODO
            intensities = intensities - np.mean(intensities)
            observation = np.append(fwhms, intensities)
                
            loss = loss_function_tri(bunches[0], bunches[1], bunches[2])#voltage_loss(profile) # Compute loss
            # plt.figure()
            # plt.title(f'Final loss: {loss}')
            # plt.plot(profile)
            volt_step += soln.nf
            print(f'Voltage optimised. Took {volt_step} steps to optimise.')
            tot = info['steps'] + volt_step
            print(f'Total number of steps for phase and volt: {tot}')
            steps_per_episode[episode] += volt_step


            obs = env.reset()
print(f"Succesful optimizations: {performance[0]}")
print(f"Unsuccesful optimizations: {performance[1]}")
print(f"Accuracy: {performance[0]/(performance[0]+performance[1])}")
print(f"Mean episode length: {np.sum(steps_per_episode)/len(steps_per_episode)}")
print(f"Max episode length: {np.max(steps_per_episode)}, Min episode length: {np.min(steps_per_episode)}")
print(f"Mean squared error of bls w.r.t. the three BLs mean length: {np.mean(np.array(end_se))}")

# Create saving directory
if not os.path.exists('RL_plots/tri/{}'.format(model_name)):
    os.makedirs('RL_plots/tri/{}'.format(model_name))

p14_err = []
p21_err = []
v14_final = []
for entry in predicted_phase_error_when_done:
    p14_err.append(entry[0])
    p21_err.append(entry[1])
    v14_final.append(entry[2])
plt.plot(p14_err, p21_err, '.')
plt.title('{}: Scatter plot of correction errors'.format(model_name))
plt.xlabel('p14 prediction error')
plt.ylabel('p21 prediction error')
plt.xlim(-15,15)
plt.ylim(-10,10)
plt.savefig('RL_plots/tri/{}/scatter_plot.png'.format(model_name))
plt.show()

plt.figure()
plt.hist(v14_final,21,(0.9,1.1))
plt.show()

plt.figure()
plt.title('Start and end Agent Criterion')
plt.plot(start_diff_estimates, 'ro-', label='start')
plt.plot(end_diff_estimates, 'go-', label='End')
plt.plot(np.linspace(0,len(start_diff_estimates),len(start_diff_estimates)+1),np.ones(len(start_diff_estimates)+1)*0.001, 'k--', label='Stop Criterion')
plt.xlabel('Episodes')
plt.legend()
plt.savefig('RL_plots/tri/{}/init_end_criterion.png'.format(model_name))

plt.figure()
plt.suptitle('Five initial and final profiles, SAC-simple-profile-normdiff00099')
plt.subplot(251)
plt.title('%.5f'%start_diff_estimates[0])
plt.plot(initial_profiles[0])
plt.subplot(252)
plt.title('%.5f'%start_diff_estimates[1])
plt.plot(initial_profiles[1])
plt.subplot(253)
plt.title('%.5f'%start_diff_estimates[2])
plt.plot(initial_profiles[2])
plt.subplot(254)
plt.title('%.5f'%start_diff_estimates[3])
plt.plot(initial_profiles[3])
plt.subplot(255)
plt.title('%.5f'%start_diff_estimates[4])
plt.plot(initial_profiles[4])
plt.subplot(256)
plt.title('%.5f'%end_diff_estimates[0])
plt.plot(final_profiles[0])
plt.subplot(257)
plt.title('%.5f'%end_diff_estimates[1])
plt.plot(final_profiles[1])
plt.subplot(258)
plt.title('%.5f'%end_diff_estimates[2])
plt.plot(final_profiles[2])
plt.subplot(259)
plt.title('%.5f'%end_diff_estimates[3])
plt.plot(final_profiles[3])
plt.subplot(2,5,10)
plt.title('%.5f'%end_diff_estimates[4])
plt.plot(final_profiles[4])
plt.savefig('RL_plots/tri/{}/init_end_profiles.png'.format(model_name))
plt.show()

print('Testing done.')