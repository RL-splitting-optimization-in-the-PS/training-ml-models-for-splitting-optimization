

#%% Imports
import sys
import os
file_path = os.path.realpath(__file__)
dirname=os.path.dirname(file_path)
parent=os.path.dirname(dirname)
sys.path.append(f'{parent}')
from mpl_toolkits import mplot3d

import random
import numpy as np
import gym
import matplotlib.pyplot as plt
from numpy.core.function_base import linspace
from numpy.lib.function_base import diff
from scipy import interpolate
from pathlib import Path
import torch
#from pytorch_fitmodule import FitModule
# from torch.autograd import Variable
import numpy as np
from torchvision import transforms, utils
# from torch.utils.data import Dataset, DataLoader
from neural_networks.dataloader import ToTensor, Normalize, AddNoise
import torch.optim as optim
import matplotlib.pyplot as plt
from datamatrix_lookup_class_quad import Datamatrix_lookup_class_quad
from utils import profile_reward_quad, isolate_bunches_from_dm_profile, loss_function_two

#from plots.enhancer import plot_finish


#%% Twosplit environment class

OFFSET = 5

REWARD_OFFSET = -1

BUNCH_LENGTH_CRITERIA = 0.04 # Empirically evaluated diff_estimate that constitutes a "good" bunch splitting. Lower means longer training time, but smaller spread in bunch lengths.
PROFILE_MSE_CRITERIA = 0.0003 # 60000
#SIMULATION_DATA = 'corrected'
SIMULATION_DATA = 'uncorrected'

#REWARD_FUNC = 'gauss'
#REWARD_FUNC = 'gauss_profile'
#REWARD_FUNC = 'gauss_linear_profile'
#REWARD_FUNC = 'gauss_profile_step'
#REWARD_FUNC = 'parabola'
#REWARD_FUNC = 'observable'
#REWARD_FUNC = 'simple'
REWARD_FUNC = 'simple_profile'
#REWARD_FUNC = 'profile'
#REWARD_FUNC = 'MSE'

ACTION_TYPE = 'relative'
#ACTION_TYPE = 'absolute'




transform=transforms.Compose([
            Normalize(),
            ToTensor(),
            AddNoise(),
        ])

# Loading simulation data

data_dir = r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl"

data_class = Datamatrix_lookup_class_quad() # data_class.get_interpolated... gets interp. datamatrix for CNN

phase = np.linspace(-30,30,121) #loaded_results['dpc20'] + OFFSET

min_setting=np.min(phase)
max_setting=np.max(phase)



# Normalize peak_mod_int

#peak_mod_int = peak_mod_int/max_spread



class QuadEnvBLInt(gym.Env):
    """
    Environment class for the optimization of the h=84 double splitting as part of the quadruple split.
    Trained using tomoscope-like data for the quadruple split, extracting the final turn with four bunches
    and calculating bunch length (fwhm) and bunch intensities (in a simplified way). Could be improved by
    changing the input data to something more specific to this double split.
    """

    phase_set = 0
    


    action_space = gym.spaces.Box(
            np.array([-1]), # change in p42 to apply. Propagated to p84 to decouple phases.
            np.array([1]),
            shape=(1,),
            dtype=np.float32)


    ### Define what the observations are to be expected
    observation_space = gym.spaces.Box(
            np.array([-1, -1, -1, -1]), # Rel Bunch lengths and intensity after h=42 splitting.
            np.array([1, 1, 1, 1]),
            shape=(4,),
            dtype=np.float32)
    
    ### Observable/State

    state = [0, 0]
    action = 0
    diff_estimate = None
    extracted_feats = None

    ### Evaluation info
    phase_correction = 0
    initial_offset = 0

    



    diff_estimate = 0

    reward_width = 0.025


    ### Status of the iterations
    # Steps, i.e. number of cycles
    counter = 0
    curr_step = -1  ## Not used ?
    
    
    # Episodes, i.e. number of MDs, number of LHC fills...
    curr_episode = -1
    action_episode_memory = []
    state_memory = []
    phase_set_memory = []
    reward_memory = []
    is_finalized = False
    diff_estimate_memory = []


    def __init__(self, max_step_size=20,
                 min_setting=min_setting, max_setting=max_setting,
                 #min_spread=min_spread, max_spread=max_spread, 
                 max_steps=100,
                 seed=None, 
                 cnn_model_name='cnn_quad_ms_ADD_NOISE_Move_injectionApr_28_2022_12-28-32_e29_continue__e46', 
                 using_tanh=False):
        """
        Initialize the environment
        """
        ### Settings
        self.min_setting = min_setting
        self.max_setting = max_setting
        ### Define what the agent can do
        self.max_step_size = max_step_size
        self.max_steps = max_steps
   
        ### Set the seed
        self.seed(seed)
        
        ### Reset
        #self.reset()
        
        
    def step(self, action):
        """
        One step/action in the environment, returning the observable
        and reward.

        Stopping conditions: max_steps reached, or small enough difference between bunch lengths reached (BUNCH_LENGTH_CRITERIA)
        """
        success = False    
        self.curr_step += 1
        #print(f" state before action {self.state}, action {action}")
        self._take_action(action)
        #print(f" state After action {self.state}")
        reward = self._get_reward()
        curr_diff_estimate = self.diff_estimate.copy()
        self.diff_estimate_memory[self.curr_episode].append(curr_diff_estimate)
        state = self.state
        if REWARD_FUNC == 'profile' or REWARD_FUNC == 'simple_profile' or REWARD_FUNC == 'gauss_profile' or REWARD_FUNC == 'gauss_profile_step' or REWARD_FUNC == 'gauss_linear_profile':
            if REWARD_FUNC=='gauss_linear_profile':
                if abs(self.diff_estimate) < PROFILE_MSE_CRITERIA+(PROFILE_MSE_CRITERIA*0.25):
                    self.is_finalized = True
                    success = True
            else:
                if abs(self.diff_estimate) < PROFILE_MSE_CRITERIA:
                    self.is_finalized = True
                    success = True
        else:
            if abs(self.diff_estimate) < BUNCH_LENGTH_CRITERIA:
                self.is_finalized = True
                success = True
            #reward = reward + 1 # Reward early succesful optimization?
        if self.counter >= self.max_steps:
            self.is_finalized = True

            ### Don't allow it to go too far from the data
        #if (self.phase_set<self.min_setting-20) or (self.phase_set>self.max_setting+20):
        #    self.is_finalized = True
        #reward = reward + state[1] # Add bonus if observable shrinks, minus otherwise
        self.reward_memory[self.curr_episode].append(reward)
        info = {'success': success, 'steps': self.counter, 'phase_corr': self.phase_correction, 'initial_phase': self.initial_offset, 'diff_estimate': self.diff_estimate, 'profile': self.profile}
        
        return state, reward, self.is_finalized, info
    
    def _take_action(self, action):
        """
        Actual action funtion.

        Action from model is scaled to be between [-1,1] for better optimization performance. 
        Converted back to phase setting in degrees using self.max_step_size.
        """
        
        #action = action[0]
        self.action = action
        converted_action = action*self.max_step_size
        self.phase_correction += converted_action

        # Phase offset as action, Best action for DDPG
        if ACTION_TYPE == 'relative':
            self.phase_set += converted_action
        
        # Absolute phase as action, Best action for NAF
        elif ACTION_TYPE == 'absolute':
            self.phase_set = converted_action
    
        self.state = self._get_state()
        curr_state = self.state.copy()
        curr_phase_set = self.phase_set.copy()
        self.action_episode_memory[self.curr_episode].append(action)
        self.state_memory[self.curr_episode].append(curr_state)
        self.phase_set_memory[self.curr_episode].append(curr_phase_set)
        
        self.counter += 1
    
    def _get_state(self):
        '''
        Get the observable for a given phase_set

        Comment: The edge cases handling datapoints outside the simulated data should be investigated more, and perhaps more cases need to be added.
        The important factors considered when making it as it is now was to make sure that all edge cases are covered by some state, and
        that the different edge cases lead to different states (so the model can learn what steps to take in what areas). In this way,
        the RL agent should learn to at least step back in the simulated area when going outside.
        '''

        if (self.phase_set<self.min_setting):
            state = np.array([0.5, -0.5, 0.5, -0.5])
        elif (self.phase_set>self.max_setting):
            state = np.array([-0.5, 0.5, -0.5, 0.5])
        else:
            # Interpolating the state/observable from the simulated data
            datamatrix = data_class.get_interpolated_matrix(self.phase_42, self.phase_set) # Second phase does not affect the first
            #self.profile = data_class.get_interpolated_profile(self.phase_set[0], 0)
            # Convert input into normalized tensor
            sample = {}
            sample['image'] = datamatrix
            sample['labels'] = np.array([0,0])
            transformed_sample = transform(sample)
            input = transformed_sample['image']
            self.profile = input[0,-1,:].numpy() # Final profile after both splittings

            ###############
            # CALCULATE BUNCH LENGTHS AND INTENSITIES FROM THE PROFILE
            ###############
            self.bunches, fwhms, intensities = isolate_bunches_from_dm_profile(self.profile, intensities=True, bunch_width=8, rel=True, plot_found_bunches=False, distance=20)
            
            # Four bunches found, take average of 1,3 and 2,4 respectively
            self.bunches = [(self.bunches[0] + self.bunches[2])/2, (self.bunches[1]+self.bunches[3])/2]

            fwhms = [(fwhms[0] + fwhms[2])/2, (fwhms[1]+fwhms[3])/2]
            intensities = [(intensities[0] + intensities[2])/2, (intensities[1]+intensities[3])/2]

            fwhms = fwhms -np.mean(fwhms)
            # How to normalize intensities to be in range [0,1]? For now, divide by constant.
            intensities = intensities / max(intensities) # Check that 50 is reasonable #TODO
            intensities = intensities - np.mean(intensities)
            self.fwhms, self.intensities = fwhms, intensities

            #print(input.shape)
            # outputs = self.feat_extr(input).detach().numpy()/100 # Output of NN is phase offset * 100
            # outputs[0][2] = outputs[0][2]/(10) # Voltage should be divided by 1000 in total
            # self.extracted_feats = outputs
            # #NORMALIZE PHASES and VOLTAGES FROM NN between 0 and 1:
            # outputs[0][:2] = outputs[0][:2]/self.max_setting
            # outputs[0][2] = (outputs[0][2]-1)/(self.max_volt-1) # v14 [0.9,1.1]. -1 to center around zero, divide by 0.1 to make between [-1,1]
            #bl1_3 = np.append(fwhms[0], fwhms[2])
            #int1_3 = np.append(intensities[0], intensities[2])
            bls_and_intensities = np.append(fwhms, intensities)
            #print(bls_and_intensities)
            state = bls_and_intensities
        
        return state
    
    def _get_reward(self):
        '''
        Evaluating the reward from the observable

        TESTED FUNCTIONS: Gauss, Simple. Others may be deprecated.
        '''
        
        observable = self.state[:4]
        # Best rewards for DDPG/NAF
        if REWARD_FUNC == 'observable':
            reward = -self.observable_sim_extended(self.phase_set)# + 0.25
        if REWARD_FUNC == 'gauss':
            diff_estimate = 0
            for bunch1 in observable:
                for bunch2 in observable:
                    diff_estimate += abs(bunch1 - bunch2)
            self.diff_estimate = diff_estimate
            if (self.phase_set[0]<self.min_setting) or (self.phase_set[0]>self.max_setting) or (self.phase_set[1]<self.min_setting) or (self.phase_set[1]>self.max_setting):
                reward = np.exp(-(diff_estimate/(np.sqrt(2)*self.reward_width))**2)+REWARD_OFFSET-1
            else:
                reward = np.exp(-(diff_estimate/(np.sqrt(2)*self.reward_width))**2)+REWARD_OFFSET
            if abs(diff_estimate) < BUNCH_LENGTH_CRITERIA: # Tested to see where it reaches few degree accuracy
                reward += 10
                
        elif REWARD_FUNC == 'parabola':
            if (self.phase_set<self.min_setting) or (self.phase_set>self.max_setting):
                reward = 1-(observable/(3*self.reward_width))**2 - 1 + REWARD_OFFSET
            else:
                reward = 1-(observable/(3*self.reward_width))**2 + REWARD_OFFSET
        
        elif REWARD_FUNC == 'linear':
            if (self.phase_set<self.min_setting) or (self.phase_set>self.max_setting):
                reward = -1 + REWARD_OFFSET
            else:
                reward = -np.abs(np.linspace(-45,45,91))/45 + REWARD_OFFSET
        elif REWARD_FUNC == 'simple':
            reward = -1
            diff_estimate = 0
            for bunch1 in observable:
                for bunch2 in observable:
                    diff_estimate += abs(bunch1 - bunch2)
            self.diff_estimate = diff_estimate
            if (self.phase_set[0]<self.min_setting) or (self.phase_set[0]>self.max_setting) or (self.phase_set[1]<self.min_setting) or (self.phase_set[1]>self.max_setting):
                reward += -4
            
            elif abs(diff_estimate) < BUNCH_LENGTH_CRITERIA: # Tested to see where it reaches 1 degree accuracy
                reward += 101
            elif abs(diff_estimate) < 0.2:
                reward += 0.5
        elif REWARD_FUNC == 'profile':
            
            diff_estimate = 1000000
            #profile = data_class.get_interpolated_profile(self.phase_volt_set[0], self.phase_volt_set[1], self.phase_volt_set[2])
            reward = profile_reward_quad(self.profile)
            diff_estimate = -reward
            self.diff_estimate = diff_estimate
            if (self.phase_set[0]<self.min_setting) or (self.phase_set[0]>self.max_setting) or (self.phase_set[1]<self.min_setting) or (self.phase_set[1]>self.max_setting):
                reward += -4
            
            elif abs(diff_estimate) < PROFILE_MSE_CRITERIA: # Tested to see where it is close to optimal setting
                reward += 101
            # elif abs(diff_estimate) < 0.02:
            #     reward += 0.25
            # elif abs(diff_estimate) < 0.01:
            #     reward += 0.5
            # elif abs(diff_estimate) < 0.001:
            #     reward += 0.75


        elif REWARD_FUNC == 'simple_profile':
            reward = -1
            diff_estimate = 1000000
            #profile = data_class.get_interpolated_profile(self.phase_volt_set[0], self.phase_volt_set[1], self.phase_volt_set[2])
            diff_estimate = loss_function_two(self.bunches[0], self.bunches[1])
            self.diff_estimate = diff_estimate
            if (self.phase_set<self.min_setting) or (self.phase_set>self.max_setting):
                reward += -4
            
            elif abs(diff_estimate) < PROFILE_MSE_CRITERIA: # Tested to see where it is close to optimal setting
                reward += 101
            elif abs(diff_estimate) < 0.0012:
                reward += 0.75
            elif abs(diff_estimate) < 0.01:
                reward += 0.5
            elif abs(diff_estimate) < 0.02:
                reward += 0.25
        elif REWARD_FUNC == 'gauss_profile':
            diff_estimate = 1000000
            #profile = data_class.get_interpolated_profile(self.phase_volt_set[0], self.phase_volt_set[1], self.phase_volt_set[2])
            diff_estimate = -profile_reward_quad(self.profile)
            self.diff_estimate = diff_estimate
            if (self.phase_set[0]<self.min_setting) or (self.phase_set[0]>self.max_setting) or (self.phase_set[1]<self.min_setting) or (self.phase_set[1]>self.max_setting):
                reward = np.exp(-(diff_estimate/(np.sqrt(2)*self.reward_width))**2)+REWARD_OFFSET-1
            else:
                reward = np.exp(-(diff_estimate/(np.sqrt(2)*self.reward_width))**2)+REWARD_OFFSET
            if abs(diff_estimate) < PROFILE_MSE_CRITERIA: # Tested to see where it reaches few degree accuracy
                reward += 10 # try 0.1?
        elif REWARD_FUNC == 'gauss_linear_profile':
            diff_estimate = 1000000
            #profile = data_class.get_interpolated_profile(self.phase_volt_set[0], self.phase_volt_set[1], self.phase_volt_set[2])
            diff_estimate = -profile_reward_quad(self.profile)
            self.diff_estimate = diff_estimate
            if (self.phase_set[0]<self.min_setting) or (self.phase_set[0]>self.max_setting) or (self.phase_set[1]<self.min_setting) or (self.phase_set[1]>self.max_setting):
                reward = np.exp(-(diff_estimate/(np.sqrt(2)*self.reward_width))**2)+diff_estimate*0.25+REWARD_OFFSET-1
            else:
                reward = np.exp(-(diff_estimate/(np.sqrt(2)*self.reward_width))**2)+diff_estimate*0.25+REWARD_OFFSET
            if abs(diff_estimate) < PROFILE_MSE_CRITERIA+(PROFILE_MSE_CRITERIA*0.25): # Tested to see where it reaches few degree accuracy
                reward += 10 # try 0.1?
        elif REWARD_FUNC == 'gauss_profile_step':
            """
            Trying to combine gaussian reward with a step increase when close to good values, to further encourage search in a good region. 
            """
            diff_estimate = 1000000
            #profile = data_class.get_interpolated_profile(self.phase_volt_set[0], self.phase_volt_set[1], self.phase_volt_set[2])
            diff_estimate = -profile_reward_quad(self.profile)
            self.diff_estimate = diff_estimate
            if (self.phase_set[0]<self.min_setting) or (self.phase_set[0]>self.max_setting) or (self.phase_set[1]<self.min_setting) or (self.phase_set[1]>self.max_setting):
                reward = np.exp(-(diff_estimate/(np.sqrt(2)*self.reward_width))**2)+REWARD_OFFSET-2
            elif abs(diff_estimate) > 0.002:
                reward = np.exp(-(diff_estimate/(np.sqrt(2)*self.reward_width))**2)+REWARD_OFFSET-1
            else:
                reward = np.exp(-(diff_estimate/(np.sqrt(2)*self.reward_width))**2)+REWARD_OFFSET
            if abs(diff_estimate) < PROFILE_MSE_CRITERIA: # Tested to see where it reaches few degree accuracy
                reward += 100
        return reward
       
    def reset(self):
        """
        Reset to a random state to start over the training
        """
        
        # Resetting to start a new episode
        self.curr_episode += 1
        self.counter = 0
        self.is_finalized = False

        # Initializing list to save steps for that episode
        self.action_episode_memory.append([])
        self.state_memory.append([])
        self.phase_set_memory.append([])
        self.reward_memory.append([])
        self.diff_estimate_memory.append([])
       
        # Getting initial state
        self.phase_set = np.array(0.0)
        self.phase_set = random.uniform(self.min_setting,
                                            self.max_setting)
        self.phase_42 = random.uniform(0,
                                        0)
        self.initial_offset = np.copy(self.phase_set)
        self.phase_correction = 0
        init_phase = self.phase_set.copy()      
        #self.prev_state = self.phase_set + 5
        self.state = self._get_state()
        state = self.state.copy()


        diff_estimate = loss_function_two(self.bunches[0],self.bunches[1])

        self.state_memory[self.curr_episode].append(state)
        self.phase_set_memory[self.curr_episode].append(init_phase)
        
        reward = self._get_reward()
        self.reward_memory[self.curr_episode].append(reward)
        self.diff_estimate_memory[self.curr_episode].append(diff_estimate)

        
#        if self.curr_episode == 2:
#            plt.figure('Observable')
#            plt.savefig('observable_init.png')
#            plt.figure('Reward')
#            plt.savefig('reward_init.png')
            
        

        return state

    def set_state(self, phase):

        """
        Reset environment with a specified phase setting. Used to evaluate performance when starting at specified phase offsets.
        """
        self.phase_set = phase
        self.initial_offset = np.copy(self.phase_set)

        self.action_episode_memory.append([])
        self.state_memory.append([])
        self.phase_set_memory.append([])
        self.reward_memory.append([])

        self.state = self._get_state()
        state = self.state

        self.state_memory[self.curr_episode].append(state)
        self.phase_set_memory[self.curr_episode].append(self.phase_set)
        
        reward = self._get_reward()
        self.reward_memory[self.curr_episode].append(reward)

        return state

    def seed(self, seed=None):
        """
        Set the random seed
        """
        
        random.seed(seed)
        np.random.seed
        
    def render(self, mode='human'):
        
        """
        Render walking through phase space with voltage on the side and current profile/loss alongside.
        """
        if mode=='human':
            plt.figure('Observable')
            plt.clf()
            
            
            x=[]
            y=[]
            z=[]
            for phase_set in self.phase_set_memory[self.curr_episode]:
                x.append(phase_set)
            plt.subplot(231)
            plt.ylim((-30,30))
            plt.xlabel('step')
            plt.ylabel('phase 84')
            plt.plot(x, 'yo-')


            plt.subplot(232)
            plt.title(f'Loss: {self.diff_estimate:4f}')
            plt.plot(self.profile)

            plt.subplot(233)
            plt.title("Loss")
            plt.plot(self.diff_estimate_memory[self.curr_episode], 'o-')
            length = len(self.diff_estimate_memory[self.curr_episode])
            plt.plot(np.linspace(0,length,length+1),np.ones(length+1)*PROFILE_MSE_CRITERIA, 'k--', label='Stop Criterion')
            # plt.subplot(335)
            # plt.title("tomo acq.")
            # plt.imshow(datamatrix, aspect='auto')
            plt.subplot(234)
            plt.plot(self.bunches[0], label='b1')
            plt.plot(self.bunches[1], label='b2')
            plt.subplot(235)
            plt.ylim((-0.3,0.3))
            plt.plot(self.fwhms, 'go-', label='Rel. fwhms')
            # normalize intensities
            plt.plot(self.intensities-np.mean(self.intensities), 'bo-', label='Rel. intensity')
            plt.legend()
            

                
            #plot_finish(fig=fig, axes=axes, xlabel='Setting', ylabel='Observable')
            plt.pause(0.1)
        elif mode=='console':
            pass

        
        
        







