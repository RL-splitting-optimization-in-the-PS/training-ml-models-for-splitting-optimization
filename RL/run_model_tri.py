#%%
from tracemalloc import start
import gym
import numpy as np
from numpy.core.fromnumeric import mean, shape
from numpy.core.function_base import linspace
from sklearn.metrics import mean_squared_error
import json
#from tri_env_bl_cnn import BUNCH_LENGTH_CRITERIA
#from tri_env_bl_int_cnn import TriEnvBLIntCNN
#from tri_env_rel_bl_int_cnn_phase_loss import TriEnvRelBLIntCNNPhase
#from tri_env_rel_bl_int_phase_loss import TriEnvRelBLIntPhase

from tri_env_rel_bl_int import TriEnvRelBLInt

from stable_baselines3 import SAC
from stable_baselines3 import TD3
from stable_baselines3 import DDPG
from stable_baselines3 import her
#from stable_baselines3.common.policies import FeedForwardPolicy

from stable_baselines3.common.results_plotter import load_results, ts2xy
#from stable_baselines3 HerReplayBuffer
import matplotlib.pyplot as plt

import os
# SAC-BLIntCnn-Simple-profile-59521-ref-NoiseMoveInjPV-diff-0001
# SAC-CNN-only-step001-Simple-profile-VPC-rew-59521-ref-NoiseMoveInjPV-diff-0001
# SAC-RelBLIntVolt-gauss-profile-59521-ref-NoiseMoveInjPV-diff-00009
# SAC-RelBLIntPhase-phase-loss-profile-step10-59521-ref-NoiseMoveInjPV-diff-00012
# SAC-RelBLIntVolt-voltloss-step01-profile-59521-ref-NoiseMoveInjPV-diff-00010 # Voltage with step 0.1

model_name = r"SAC-RelBLInt-allparams-sprof-step10-002-59521-ref-NoiseMoveInj-diff-00008_900k"
model = SAC.load("./RL_logs/tri/{}".format(model_name))
env = TriEnvRelBLInt(max_step_size=10, max_step_volt=0.1)

obs = env.reset()
performance = [0,0]
steps_per_episode = []
predicted_phase_error_when_done = []
end_bunch_lengths = []
end_se = []
start_diff_estimates = []
end_diff_estimates = []
initial_profiles = []
final_profiles = []
counter = 0
mode='human' # 'console'
render_opt = False
for episode in range(50):
    done=False
    first_step=True
    obs, rewards, _, info_start = env.step([0,0,0]) # Do nothing action to get init state
    start_diff_estimates.append(info_start['diff_estimate'])
    print(f'episode {episode}: start_diff {start_diff_estimates[episode]}')
    if counter < 5:
        initial_profiles.append(info_start['profile'])
    
    while not done:
        action, _states = model.predict(obs, deterministic=True)
        #print(action*10)
        obs, rewards, done, info = env.step(action)
        #print(f'Observation: {obs}')
        if render_opt:
            env.render(mode=mode) # Use if you want to observe the agent
        first_step=False
        if done and not info['success']:
            print('hej')
        if done:
            print(f"Took {info['steps']-1} steps before terminating test")
            print(f"Info {info['success']}, {info['steps']-1}, {info['initial_phase']}, {info['phase_corr']}")
            steps_per_episode.append(info['steps']-1)
            end_diff_estimates.append(info['diff_estimate'])
            print(f'episode {episode}: end_diff {end_diff_estimates[episode]}')
            if info['success'] == True:
                performance[0] += 1
            else:
                performance[1] += 1
            end_bunch_lengths.append(obs[:3])

            bls = np.array(obs[:3])
            target = np.mean(bls)
            end_se.append(np.sum((bls-target)**2)/len(bls))

            predicted_phase_error_when_done.append(info['initial_phase'] + info['phase_corr'])
            if counter < 5:
                final_profiles.append(info['profile'])
            counter += 1
            obs = env.reset()
print(f"Succesful optimizations: {performance[0]}")
print(f"Unsuccesful optimizations: {performance[1]}")
print(f"Accuracy: {performance[0]/(performance[0]+performance[1])}")
print(f"Mean episode length: {np.sum(steps_per_episode)/len(steps_per_episode)}")
print(f"Max episode length: {np.max(steps_per_episode)}, Min episode length: {np.min(steps_per_episode)}")
print(f"Mean squared error of bls w.r.t. the three BLs mean length: {np.mean(np.array(end_se))}")

# Create saving directory
if not os.path.exists('RL_plots/tri/{}'.format(model_name)):
    os.makedirs('RL_plots/tri/{}'.format(model_name))

p14_err = []
p21_err = []
v14_final = []
for entry in predicted_phase_error_when_done:
    p14_err.append(entry[0])
    p21_err.append(entry[1])
    v14_final.append(entry[2])
plt.plot(p14_err, p21_err, '.')
plt.title('{}: Scatter plot of correction errors'.format(model_name))
plt.xlabel('p14 prediction error')
plt.ylabel('p21 prediction error')
plt.xlim(-15,15)
plt.ylim(-10,10)
plt.savefig('RL_plots/tri/{}/scatter_plot.png'.format(model_name))
plt.show()

plt.figure()
plt.hist(v14_final,21,(0.9,1.1))
plt.show()

plt.figure()
plt.title('Start and end Agent Criterion')
plt.plot(start_diff_estimates, 'ro-', label='start')
plt.plot(end_diff_estimates, 'go-', label='End')
plt.plot(np.linspace(0,len(start_diff_estimates),len(start_diff_estimates)+1),np.ones(len(start_diff_estimates)+1)*0.0008, 'k--', label='Stop Criterion')
plt.xlabel('Episodes')
plt.legend()
plt.savefig('RL_plots/tri/{}/init_end_criterion.png'.format(model_name))

plt.figure()
plt.suptitle('Five initial and final profiles, {}'.format(model_name))
plt.subplot(251)
plt.title('%.5f'%start_diff_estimates[0])
plt.plot(initial_profiles[0])
plt.subplot(252)
plt.title('%.5f'%start_diff_estimates[1])
plt.plot(initial_profiles[1])
plt.subplot(253)
plt.title('%.5f'%start_diff_estimates[2])
plt.plot(initial_profiles[2])
plt.subplot(254)
plt.title('%.5f'%start_diff_estimates[3])
plt.plot(initial_profiles[3])
plt.subplot(255)
plt.title('%.5f'%start_diff_estimates[4])
plt.plot(initial_profiles[4])
plt.subplot(256)
plt.title('%.5f'%end_diff_estimates[0])
plt.plot(final_profiles[0])
plt.subplot(257)
plt.title('%.5f'%end_diff_estimates[1])
plt.plot(final_profiles[1])
plt.subplot(258)
plt.title('%.5f'%end_diff_estimates[2])
plt.plot(final_profiles[2])
plt.subplot(259)
plt.title('%.5f'%end_diff_estimates[3])
plt.plot(final_profiles[3])
plt.subplot(2,5,10)
plt.title('%.5f'%end_diff_estimates[4])
plt.plot(final_profiles[4])
plt.savefig('RL_plots/tri/{}/init_end_profiles.png'.format(model_name))
plt.show()

plt.figure()
plt.title('Steps per episode')
plt.plot(steps_per_episode, 'go-', label='Nbr of steps')
plt.plot(np.linspace(0,len(steps_per_episode),len(steps_per_episode)+1),np.ones(len(steps_per_episode)+1)*np.mean(steps_per_episode), 'k--', label='Mean steps')
plt.xlabel('Episodes')
plt.legend()
plt.savefig('RL_plots/tri/{}/steps.png'.format(model_name))

saving_dict = {}
saving_dict['steps'] = performance
saving_dict['actions'] = start_diff_estimates
saving_dict['dms'] = end_diff_estimates
saving_dict['profile_memory'] = final_profiles
saving_dict['loss_memory'] = initial_profiles
saving_dict['rel_phase_volt_set'] = steps_per_episode

episode_save_dir = r'C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\test_save_to_json'
json_file = json.dumps(saving_dict,  sort_keys=True, indent=4, separators=(',', ': '))
with open(episode_save_dir + r'\episode_data.json', "w") as file:
    file.write(json_file)

print('Testing done.')