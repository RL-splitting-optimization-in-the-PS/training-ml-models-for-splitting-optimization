# -*- coding: utf-8 -*-
"""
Class defining the possible actions for the agent to do and the corresponding
reward
"""

#%% Imports

import sys
import os
file_path = os.path.realpath(__file__)
dirname=os.path.dirname(file_path)
parent=os.path.dirname(dirname)
sys.path.append(f'{parent}')
import random
import numpy as np
import gym
import matplotlib.pyplot as plt
from numpy.core.function_base import linspace
from numpy.lib.function_base import diff
from scipy import interpolate
from pathlib import Path
import torch
#from pytorch_fitmodule import FitModule
# from torch.autograd import Variable
import numpy as np
from torchvision import transforms, utils
# from torch.utils.data import Dataset, DataLoader
from neural_networks.dataloader import ToTensor, Normalize, AddNoise
import torch.optim as optim
import matplotlib.pyplot as plt
from datamatrix_lookup_class_tri import Datamatrix_lookup_class_tri
from utils import profile_reward_tri, isolate_bunches_from_dm_profile_tri, loss_function_tri, tri_phase_loss


#%% Twosplit environment class

OFFSET = 5

REWARD_OFFSET = -1

VPC_REWARD = True

BUNCH_LENGTH_CRITERIA = 0.06 # Empirically evaluated diff_estimate that constitutes a "good" bunch splitting. Lower means longer training time, but smaller spread in bunch lengths.
PROFILE_MSE_CRITERIA = 0.0012  #0.0009 #0.001 #0.0098#60000
BL_INT_PHASE_LOSS_CRITERIA = 0.0005
PHASE_LOSS_CRITERIA = 0.0012
VOLT_LOSS_CRITERIA = 0.0010

ACTION_TYPE = 'relative'

transform=transforms.Compose([
            Normalize(),
            ToTensor(),
            AddNoise(),
        ])

# Loading simulation data

#data_dir = r"/eos/project/s/sy-rf-br/Machines/PS/DatasetsForML/Trisplit/machine_settings_fixed"
data_dir = r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl"

data_class = Datamatrix_lookup_class_tri(only35k=True) # data_class.get_interpolated... gets interp. datamatrix for CNN

# loaded_results = np.load(data_dir + '/lookup_table_bunch_lengths_tri_ms_fixed.npy')


phase = np.linspace(-20,20,41) #loaded_results['dpc20'] + OFFSET
volt = np.linspace(0.95,1.05,21)
points = (phase, phase, volt)

# bunch_lengths = loaded_results # Bunch lengths are normalized between 0 and 1.


# def interpolation(x, y, z, points = points, lookup_table=loaded_results):  # Interpolating function
#     values1 = lookup_table[:,:,:,0]
#     values2 = lookup_table[:,:,:,1]
#     values3 = lookup_table[:,:,:,2]
    
#     return_array = interpolate.interpn(points, values1, np.array([x, y, z]))
#     return_array = np.append(return_array, interpolate.interpn(points, values2, np.array([x, y, z])))
#     return_array = np.append(return_array, interpolate.interpn(points, values3, np.array([x, y, z])))
#     return return_array

min_setting=np.min(phase)
max_setting=np.max(phase)
#min_spread=np.min(bunch_lengths)
#max_spread=np.max(bunch_lengths)
max_volt = np.max(volt)

# Normalize peak_mod_int

#peak_mod_int = peak_mod_int/max_spread


class TriEnvRelBLIntVoltage(gym.Env):
    """
    Environment class for the triple split optimization (action and reward) using simulated data.

    The goal is to minimize the differences between the three final bunches by adjusting the 
    phase offsets of the h=14 and h=21 cavities as well as altering the voltage program for the h=14 cavity.
    """
    ### Domain declaration
    action_space = gym.spaces.Box(
                        np.array([-1]),
                        np.array([1]),
                        shape=(1,),
                        dtype=np.float32)
    observation_space = gym.spaces.Box(
                        np.array([-1, -1, -1, -1, -1, -1]), # RELATIVE! bl1, bl2, bl3, int1, int2, int3
                        np.array([1, 1, 1, 1, 1, 1]),
                        shape=(6,),
                        dtype=np.float32)
    

    objective = PROFILE_MSE_CRITERIA
    max_objective = PROFILE_MSE_CRITERIA # Currently not using external reward dangling, reward is strictly defined in this class.

    ### Available Reward functions
    REWARD_FUNC = 'simple_profile' # default reward function
    reward_functions = [
        'gauss', #0
        'gauss_profile', #1
        'gauss_linear_profile', #2
        'gauss_profile_step', #3
        'simple', #4
        'simple_profile', #5
        'profile', #6
        'MSE', #7
        'simple_gauss_end_profile', #8
        'bl_int_phase_loss',
        'phase_loss',
        'volt_loss'
    ]
    ### Status of the iterations
    # Steps, i.e. number of cycles
    counter = 0
    curr_step = -1  ## Not used ?
    
    
    ### Episodes, i.e. number of MDs, number of LHC fills...
    ### Initialise variables/lists to be tracked.
    diff_estimate = None
    diff_estimate_memory = []
    extracted_feats = None
    curr_episode = -1
    action_episode_memory = []
    state_memory = []
    phase_volt_set_memory = []
    reward_memory = []
    is_finalized = False
    

    ### Evaluation info
    phase_correction = 0
    initial_offset = 0
    def __init__(self, max_step_size=10,
                 min_setting=min_setting, max_setting=max_setting,
                 #min_spread=min_spread, 
                 max_volt=max_volt, 
                 max_step_volt = 0.02,
                 #max_spread=max_spread,
                 max_steps=100,
                 cnn_model_name='cnn_tri_Move_Injection4_PV_59521_ref_voltageJun_23_2022_13-23-23_e41',
                 reward_function=11,
                 seed=None,
                 using_tanh=False):
        """
        Initialize the environment
        """

        # ### LOAD FEATURE EXTRACTING CNN

        # self.feat_extr = CNN_tri()
        # self.optimizer = optim.Adam(self.feat_extr.parameters(), lr=1e-3)
        # self.model_name = cnn_model_name
        # self.using_tanh = using_tanh
        # print(f'Fetching model {self.model_name}...')
        # path = data_dir + r"\saved_models\{}.pth".format(self.model_name)

        # #path = r"/eos/home-j/jwulff/workspaces/RL-007-RFinPS/optimising-rf-manipulations-in-the-ps-through-rl/saved_models/{}.pth".format(self.model_name)
        # checkpoint = torch.load(path)
        # self.feat_extr.load_state_dict(checkpoint['model_state_dict'])
        # self.optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        # criterion = checkpoint['loss']
        # print(f'Weights loaded...')
        # self.feat_extr.eval()
        
        ### Settings
        self.min_setting = min_setting
        self.max_setting = max_setting
        self.max_volt = max_volt
        self.phase_volt_set = [0.0,0.0,1.0]
        self.max_step_volt = max_step_volt
        self.max_steps = max_steps
        self.max_step_size = max_step_size
        
        self.REWARD_FUNC = self.reward_functions[reward_function]

        ### Assign reward width (standard deviation) for gaussian style rewards
        self.reward_width = 0.025

        ### Set the seed
        self.seed(seed)
        
        ### Reset
        #self.reset()
        
        
    def step(self, action):
        """
        One step/action in the environment, returning the observable
        and reward.

        Stopping conditions: max_steps reached, or small enough difference between bunch lengths reached (BUNCH_LENGTH_CRITERIA)
        """
        success = False 
        #print(f' phase volt {self.phase_volt_set_memory[self.curr_episode]}')   
        self.curr_step += 1
        #print(f" state before action {self.state}, action {action}")
        self._take_action(action)
        #print(f" state After action {self.state}")
        reward = self._get_reward()
        curr_diff_estimate = self.diff_estimate.copy()
        self.diff_estimate_memory[self.curr_episode].append(curr_diff_estimate)


        state = self.state
        if self.REWARD_FUNC == 'profile' or self.REWARD_FUNC == 'simple_profile' or self.REWARD_FUNC == 'gauss_profile' or self.REWARD_FUNC == 'gauss_linear_profile' or self.REWARD_FUNC == 'simple_gauss_end_profile':
            if abs(self.diff_estimate) < PROFILE_MSE_CRITERIA:
                self.is_finalized = True
                success = True
        elif self.REWARD_FUNC == 'phase_loss':
            if abs(self.diff_estimate) < PHASE_LOSS_CRITERIA:
                self.is_finalized = True
                success = True
        elif self.REWARD_FUNC == 'volt_loss':
            if abs(self.diff_estimate) < VOLT_LOSS_CRITERIA:
                self.is_finalized = True
                success = True
        else:
            if abs(self.diff_estimate) < BUNCH_LENGTH_CRITERIA:
                self.is_finalized = True
                success = True
            #reward = reward + 1 # Reward early succesful optimization?
        if self.counter >= self.max_steps:
            self.is_finalized = True

            ### Don't allow it to go too far from the data
        #if (self.phase_set<self.min_setting-20) or (self.phase_set>self.max_setting+20):
        #    self.is_finalized = True
        #reward = reward + state[1] # Add bonus if observable shrinks, minus otherwise
        self.reward_memory[self.curr_episode].append(reward)
        info = {'success': success, 'steps': self.counter, 'phase_corr': self.phase_correction, 'initial_phase': self.initial_offset, 'diff_estimate': self.diff_estimate, 'profile': self.profile}
        
        return state, reward, self.is_finalized, info
    
    def _take_action(self, action):
        """
        Actual action funtion.

        Action from model is scaled to be between [-1,1] for better optimization performance. 
        Converted back to phase setting in degrees using self.max_step_size.
        """
        
        #action = action[0]
        self.action = action
        converted_action = np.array([0.0,0.0,0.0])
        #converted_action[0] = action[0]*self.max_step_size
        #converted_action[1] = action[1]*self.max_step_size
        converted_action[2] = action*self.max_step_volt
        self.phase_correction += converted_action

        # Phase offset as action, Best action for DDPG
        if ACTION_TYPE == 'relative':
            self.phase_volt_set += converted_action
        
        # # Absolute phase as action, Best action for NAF
        # elif ACTION_TYPE == 'absolute':
        #     self.phase_set = converted_action
    
        self.state = self._get_state()
        current_state = self.state.copy()
        current_phase_volt_set = self.phase_volt_set.copy()
        

        self.action_episode_memory[self.curr_episode].append(action)
        self.state_memory[self.curr_episode].append(current_state)
        self.phase_volt_set_memory[self.curr_episode].append(current_phase_volt_set)
        
        self.counter += 1
    
    def _get_state(self):
        '''
        Get the observable for a given phase_set

        Comment: The edge cases handling datapoints outside the simulated data should be investigated more, and perhaps more cases need to be added.
        The important factors considered when making it as it is now was to make sure that all edge cases are covered by some state, and
        that the different edge cases lead to different states (so the model can learn what steps to take in what areas). In this way,
        the RL agent should learn to at least step back in the simulated area when going outside.
        '''

        # if (self.phase_volt_set[0]<self.min_setting):
        #     state = np.array([-0.015, -0.05, 0.2, 0.2, -1, 0])
        # elif (self.phase_volt_set[0]>self.max_setting):
        #     state = np.array([-0.05, -0.015, 0.1, 0.1, 1, 0])
        # elif (self.phase_volt_set[1]<self.min_setting): #TESTING
        #     state = np.array([0.1, -0.1, 0.2, -0.2, 0, -1 ])
        # elif (self.phase_volt_set[1]>self.max_setting):
        #     state = np.array([-0.1, 0.1, -0.2, 0.2, 0, 1])
        if (self.phase_volt_set[2] > 1.05):
            state = np.array([-0.01, -0.03, 0.04, -0.1, 0, 0.1])
        elif (self.phase_volt_set[2] < 0.95):
            state = np.array([0.04, -0.03, -0.01, 0.1, 0, -0.1])
        else:
            # Interpolating the state/observable from the simulated data
            #bls = interpolation(self.phase_volt_set[0], self.phase_volt_set[1], self.phase_volt_set[2])
            datamatrix = data_class.get_interpolated_matrix(self.phase_volt_set[0], self.phase_volt_set[1], self.phase_volt_set[2])
            
            # Convert input into normalized tensor
            sample = {}
            sample['image'] = datamatrix
            sample['labels'] = np.array([self.phase_volt_set[0], self.phase_volt_set[1], self.phase_volt_set[2]])
            transformed_sample = transform(sample)
            input = transformed_sample['image']
            self.profile = input[0,-1,:].numpy()

            ###############
            # CALCULATE BUNCH LENGTHS AND INTENSITIES FROM THE PROFILE
            ###############
            self.bunches, fwhms, intensities = isolate_bunches_from_dm_profile_tri(self.profile, intensities=True)
            fwhms = fwhms -np.mean(fwhms)
            self.fwhms, self.intensities = fwhms, intensities
            # How to normalize intensities to be in range [0,1]? For now, divide by constant.
            intensities = intensities / max(intensities) # Check that 50 is reasonable #TODO
            intensities = intensities - np.mean(intensities)
            input = input[None] # Add batch dim
            #print(input.shape)
            # outputs = self.feat_extr(input).detach().numpy()/100 # Output of NN is phase offset * 100
            # outputs[0][2] = outputs[0][2]/(10) # Voltage should be divided by 1000 in total
            # self.extracted_feats = outputs
            # #NORMALIZE PHASES and VOLTAGES FROM NN between 0 and 1:
            # outputs[0][:2] = outputs[0][:2]/self.max_setting
            # outputs[0][2] = (outputs[0][2]-1)/(self.max_volt-1) # v14 [0.9,1.1]. -1 to center around zero, divide by 0.1 to make between [-1,1]
            # bl1_3 = np.append(fwhms[0], fwhms[2])
            # int1_3 = np.append(intensities[0], intensities[2])
            bls_and_intensities = np.append(fwhms, intensities)
            state = bls_and_intensities
        return state
    
    def _get_reward(self):
        '''
        Evaluating the reward from the observable

        TESTED FUNCTIONS: Gauss, Simple. Others may be deprecated.
        '''
        
        observable = self.state[:6]
        # Best rewards for DDPG/NAF
        if self.REWARD_FUNC == 'observable':
            reward = -self.observable_sim_extended(self.phase_set)# + 0.25
        if self.REWARD_FUNC == 'gauss':
            diff_estimate = 0
            for bunch1 in observable:
                for bunch2 in observable:
                    diff_estimate += abs(bunch1 - bunch2)
            self.diff_estimate = diff_estimate
            if (self.phase_set[0]<self.min_setting) or (self.phase_volt_set[0]>self.max_setting) or (self.phase_volt_set[1]<self.min_setting) or (self.phase_volt_set[1]>self.max_setting) or (self.phase_volt_set[2]<0.9) or (self.phase_volt_set[2]>1.1):
                reward = np.exp(-(diff_estimate/(np.sqrt(2)*self.reward_width))**2)+REWARD_OFFSET-1
            else:
                reward = np.exp(-(diff_estimate/(np.sqrt(2)*self.reward_width))**2)+REWARD_OFFSET
            if abs(diff_estimate) < BUNCH_LENGTH_CRITERIA: # Tested to see where it reaches few degree accuracy
                reward += 10
                
        elif self.REWARD_FUNC == 'parabola':
            if (self.phase_set<self.min_setting) or (self.phase_set>self.max_setting):
                reward = 1-(observable/(3*self.reward_width))**2 - 1 + REWARD_OFFSET
            else:
                reward = 1-(observable/(3*self.reward_width))**2 + REWARD_OFFSET
        
        elif self.REWARD_FUNC == 'linear':
            if (self.phase_set<self.min_setting) or (self.phase_set>self.max_setting):
                reward = -1 + REWARD_OFFSET
            else:
                reward = -np.abs(np.linspace(-45,45,91))/45 + REWARD_OFFSET
        elif self.REWARD_FUNC == 'simple':
            reward = -1
            diff_estimate = 0
            for bunch1 in observable:
                for bunch2 in observable:
                    diff_estimate += abs(bunch1 - bunch2)
            self.diff_estimate = diff_estimate
            if (self.phase_volt_set[0]<self.min_setting) or (self.phase_volt_set[0]>self.max_setting) or (self.phase_volt_set[1]<self.min_setting) or (self.phase_volt_set[1]>self.max_setting) or (self.phase_volt_set[2]<0.9) or (self.phase_volt_set[2]>1.1):
                reward += -4
            
            elif abs(diff_estimate) < BUNCH_LENGTH_CRITERIA: # Tested to see where it is close to optimal setting
                reward += 101
            elif abs(diff_estimate) < 0.1:
                reward += 0.5
        elif self.REWARD_FUNC == 'profile':
            if (self.phase_volt_set[0]<self.min_setting) or (self.phase_volt_set[0]>self.max_setting) or (self.phase_volt_set[1]<self.min_setting) or (self.phase_volt_set[1]>self.max_setting) or (self.phase_volt_set[2]<0.9) or (self.phase_volt_set[2]>1.1):
                reward = -1300000 # Outside of sim 
            else:
                #profile = data_class.get_interpolated_profile(self.phase_volt_set[0], self.phase_volt_set[1], self.phase_volt_set[2])
                reward = profile_reward_tri(self.profile)
            self.diff_estimate=-reward
        elif self.REWARD_FUNC == 'MSE': # For now just a change of diff estimate, otherwise same as 'simple'. MSE error between mean of BLs and the three actual BLs.

            reward = -1
            diff_estimate = 0
            mean_bl = np.mean(observable)
            diff_estimate = (observable-mean_bl)**2 # MSE
            self.diff_estimate = diff_estimate
            if (self.phase_volt_set[0]<self.min_setting) or (self.phase_volt_set[0]>self.max_setting) or (self.phase_volt_set[1]<self.min_setting) or (self.phase_volt_set[1]>self.max_setting) or (self.phase_volt_set[2]<0.9) or (self.phase_volt_set[2]>1.1):
                reward += -4
            elif abs(diff_estimate) < BUNCH_LENGTH_CRITERIA: # Tested to see where it is close to optimal setting
                reward += 101
            elif abs(diff_estimate) < 0.1:
                reward += 0.5
        elif self.REWARD_FUNC == 'simple_profile':
            reward = -1
            diff_estimate = 1000000
            #profile = data_class.get_interpolated_profile(self.phase_volt_set[0], self.phase_volt_set[1], self.phase_volt_set[2])

            if VPC_REWARD:
                #bunches, _ = isolate_bunches_from_dm_profile_tri(self.profile)
                diff_estimate = loss_function_tri(self.bunches[0],self.bunches[1],self.bunches[2])
            else:
                diff_estimate = -profile_reward_tri(self.profile)
            self.diff_estimate = diff_estimate
            if (self.phase_volt_set[0]<self.min_setting) or (self.phase_volt_set[0]>self.max_setting) or (self.phase_volt_set[1]<self.min_setting) or (self.phase_volt_set[1]>self.max_setting) or (self.phase_volt_set[2]<0.9) or (self.phase_volt_set[2]>1.1):
                reward += -4
            
            elif abs(diff_estimate) < PROFILE_MSE_CRITERIA: # Tested to see where it is close to optimal setting
                reward += 101
            elif abs(diff_estimate) < 0.002:
                reward += 0.75
            elif abs(diff_estimate) < 0.01:
                reward += 0.5
            elif abs(diff_estimate) < 0.04:
                reward += 0.25
            # # elif abs(diff_estimate) < 0.012:
            # #     reward += 0.75
            # # elif abs(diff_estimate) < 0.02:
            # #     reward += 0.5
            # elif abs(diff_estimate) < 0.02: # 0.04:
            #     reward += 0.75
            
        elif self.REWARD_FUNC == 'gauss_profile':
            diff_estimate = 1000000
            #profile = data_class.get_interpolated_profile(self.phase_volt_set[0], self.phase_volt_set[1], self.phase_volt_set[2])
            if VPC_REWARD:
                #bunches, _ = isolate_bunches_from_dm_profile_tri(self.profile)
                diff_estimate = loss_function_tri(self.bunches[0],self.bunches[1],self.bunches[2])
            else:
                diff_estimate = -profile_reward_tri(self.profile)
            self.diff_estimate = diff_estimate
            if (self.phase_volt_set[0]<self.min_setting) or (self.phase_volt_set[0]>self.max_setting) or (self.phase_volt_set[1]<self.min_setting) or (self.phase_volt_set[1]>self.max_setting) or (self.phase_volt_set[2]<0.9) or (self.phase_volt_set[2]>1.1):
                reward = np.exp(-(diff_estimate/(np.sqrt(2)*self.reward_width))**2)+REWARD_OFFSET-1
            else:
                reward = np.exp(-(diff_estimate/(np.sqrt(2)*self.reward_width))**2)+REWARD_OFFSET
            if abs(diff_estimate) < PROFILE_MSE_CRITERIA: # Tested to see where it reaches few degree accuracy
                reward += 5
        elif self.REWARD_FUNC == 'gauss_linear_profile':
            diff_estimate = 1000000
            #profile = data_class.get_interpolated_profile(self.phase_volt_set[0], self.phase_volt_set[1], self.phase_volt_set[2])
            if VPC_REWARD:
                #bunches, _ = isolate_bunches_from_dm_profile_tri(self.profile)
                diff_estimate = loss_function_tri(self.bunches[0],self.bunches[1],self.bunches[2])
            else:
                diff_estimate = -profile_reward_tri(self.profile)
            self.diff_estimate = diff_estimate
            if (self.phase_volt_set[0]<self.min_setting) or (self.phase_volt_set[0]>self.max_setting) or (self.phase_volt_set[1]<self.min_setting) or (self.phase_volt_set[1]>self.max_setting) or (self.phase_volt_set[2]<0.9) or (self.phase_volt_set[2]>1.1):
                reward = np.exp(-(diff_estimate/(np.sqrt(2)*self.reward_width))**2)+diff_estimate*0.25+REWARD_OFFSET-1
            else:
                reward = np.exp(-(diff_estimate/(np.sqrt(2)*self.reward_width))**2)+diff_estimate*0.25+REWARD_OFFSET
            if abs(diff_estimate) < PROFILE_MSE_CRITERIA: 
                reward += 5 # try 0.1?
        elif self.REWARD_FUNC == 'simple_gauss_end_profile':
            reward = -1
            diff_estimate = 1000000
            #profile = data_class.get_interpolated_profile(self.phase_volt_set[0], self.phase_volt_set[1], self.phase_volt_set[2])

            if VPC_REWARD:
                #bunches, _ = isolate_bunches_from_dm_profile_tri(self.profile)
                diff_estimate = loss_function_tri(self.bunches[0],self.bunches[1],self.bunches[2])
            else:
                diff_estimate = -profile_reward_tri(self.profile)
            self.diff_estimate = diff_estimate
            if (self.phase_volt_set[0]<self.min_setting) or (self.phase_volt_set[0]>self.max_setting) or (self.phase_volt_set[1]<self.min_setting) or (self.phase_volt_set[1]>self.max_setting) or (self.phase_volt_set[2]<0.9) or (self.phase_volt_set[2]>1.1):
                reward += -4
            
            elif abs(diff_estimate) < PROFILE_MSE_CRITERIA: # Tested to see where it is close to optimal setting
                reward += 101
            elif abs(diff_estimate) < 0.012:
                reward += 0.75 + 0.25*np.exp(-(diff_estimate/(np.sqrt(2)*self.reward_width))**2)
            elif abs(diff_estimate) < 0.02:
                reward += 0.5
            elif abs(diff_estimate) < 0.04:
                reward += 0.25
        elif self.REWARD_FUNC == 'bl_int_phase_loss':
            # Bls and ints relative and centered around zero --> if perfect splitting, should all be 0.

            bls = observable[:2]**2
            ints = observable[2:]**2
            self.diff_estimate = np.sum(bls+ints)
            if (self.phase_volt_set[0]<self.min_setting) or (self.phase_volt_set[0]>self.max_setting) or (self.phase_volt_set[1]<self.min_setting) or (self.phase_volt_set[1]>self.max_setting) or (self.phase_volt_set[2]<0.9) or (self.phase_volt_set[2]>1.1):
                reward += -4
            
            elif abs(diff_estimate) < BL_INT_PHASE_LOSS_CRITERIA: # Tested to see where it is close to optimal setting
                reward += 101
            elif abs(diff_estimate) < 0.012:
                reward += 0.75 + 0.25*np.exp(-(diff_estimate/(np.sqrt(2)*self.reward_width))**2)
            elif abs(diff_estimate) < 0.02:
                reward += 0.5
            elif abs(diff_estimate) < 0.04:
                reward += 0.25
            reward = -self.diff_estimate
        elif self.REWARD_FUNC == 'phase_loss':
            # Bls and ints relative and centered around zero --> if perfect splitting, should all be 0.

            
            reward = -1
            diff_estimate = 1000000
            #profile = data_class.get_interpolated_profile(self.phase_volt_set[0], self.phase_volt_set[1], self.phase_volt_set[2])

            if VPC_REWARD:
                #bunches, _ = isolate_bunches_from_dm_profile_tri(self.profile)
                diff_estimate = tri_phase_loss(self.bunches[0], self.bunches[2])
            else:
                diff_estimate = -profile_reward_tri(self.profile)
            self.diff_estimate = diff_estimate
            if (self.phase_volt_set[0]<self.min_setting) or (self.phase_volt_set[0]>self.max_setting) or (self.phase_volt_set[1]<self.min_setting) or (self.phase_volt_set[1]>self.max_setting) or (self.phase_volt_set[2]<0.9) or (self.phase_volt_set[2]>1.1):
                reward += -4
            
            elif abs(diff_estimate) < PHASE_LOSS_CRITERIA: # Tested to see where it is close to optimal setting
                reward += 101
            elif abs(diff_estimate) < 0.01:
                reward += 0.75 + 0.25*np.exp(-(diff_estimate/(np.sqrt(2)*self.reward_width))**2)
            elif abs(diff_estimate) < 0.015:
                reward += 0.5
            elif abs(diff_estimate) < 0.03:
                reward += 0.25
        
        elif self.REWARD_FUNC == 'volt_loss':
            # Bls and ints relative and centered around zero --> if perfect splitting, should all be 0.

            
            reward = -1
            diff_estimate = 1000000
            #profile = data_class.get_interpolated_profile(self.phase_volt_set[0], self.phase_volt_set[1], self.phase_volt_set[2])

            
            #bunches, _ = isolate_bunches_from_dm_profile_tri(self.profile)
            diff_estimate = loss_function_tri(self.bunches[0], self.bunches[1], self.bunches[2])
            
            self.diff_estimate = diff_estimate
            if (self.phase_volt_set[0]<self.min_setting) or (self.phase_volt_set[0]>self.max_setting) or (self.phase_volt_set[1]<self.min_setting) or (self.phase_volt_set[1]>self.max_setting) or (self.phase_volt_set[2]<0.9) or (self.phase_volt_set[2]>1.1):
                reward += -4
            
            elif abs(diff_estimate) < VOLT_LOSS_CRITERIA: # Tested to see where it is close to optimal setting
                reward += 101
            elif abs(diff_estimate) < 0.0015:
                reward += 0.75 + 0.25*np.exp(-(diff_estimate/(np.sqrt(2)*self.reward_width))**2)
            elif abs(diff_estimate) < 0.01:
                reward += 0.5
            elif abs(diff_estimate) < 0.02:
                reward += 0.25
        return reward
    
    def reset(self):
        """
        Reset to a random state to start over the training
        """
        
        # Resetting to start a new episode
        self.curr_episode += 1
        self.counter = 0
        self.is_finalized = False

        # Initializing list to save steps for that episode
        self.action_episode_memory.append([])
        self.state_memory.append([])
        self.phase_volt_set_memory.append([])
        self.reward_memory.append([])
        self.diff_estimate_memory.append([])
        
        # Getting initial state
        self.phase_volt_set = np.array([0.0,0.0,0.0])

        # Allow for some phase set error, as the phase optimizer is unlikely to be perfect.
        self.phase_volt_set[0] = random.uniform(-0.25,
                                            0.25)
        self.phase_volt_set[1] = random.uniform(-1,
                                            1)
        self.phase_volt_set[2] = random.uniform(0.95,1.05)

        self.initial_offset = np.copy(self.phase_volt_set)
        self.phase_correction = 0


        init_phase_volt_set = self.phase_volt_set.copy()                  
        #self.prev_state = self.phase_set + 5
        self.state = self._get_state()
        state = self.state.copy()
        diff_estimate = loss_function_tri(self.bunches[0],self.bunches[1],self.bunches[2])
        self.state_memory[self.curr_episode].append(state)
        self.phase_volt_set_memory[self.curr_episode].append(init_phase_volt_set)
        self.diff_estimate_memory[self.curr_episode].append(diff_estimate)
        
        reward = self._get_reward()
        self.reward_memory[self.curr_episode].append(reward)
        
#        if self.curr_episode == 2:
#            plt.figure('Observable')
#            plt.savefig('observable_init.png')
#            plt.figure('Reward')
#            plt.savefig('reward_init.png')
            
        

        return state

    def set_state(self, phase_volt):

        """
        Reset environment with a specified phase setting. Used to evaluate performance when starting at specified phase offsets.
        """
        self.phase_volt_set = phase_volt
        self.initial_offset = np.copy(self.phase_volt_set)

        self.action_episode_memory.append([])
        self.state_memory.append([])
        self.phase_set_memory.append([])
        self.reward_memory.append([])

        self.state = self._get_state()
        state = self.state

        self.state_memory[self.curr_episode].append(state)
        self.phase_set_memory[self.curr_episode].append(self.phase_set)
        
        reward = self._get_reward()
        self.reward_memory[self.curr_episode].append(reward)

        return state

    def seed(self, seed=None):
        """
        Set the random seed
        """
        
        random.seed(seed)
        np.random.seed
        
    def render(self, mode='human', done=False):
        
        """
        Render walking through phase space with voltage on the side and current profile/loss alongside.
        """
        if mode=='human':
            plt.figure('Phase Optimisation')
            
            plt.clf()
            if done:
                plt.suptitle(f'Episode {self.curr_episode}: DONE!! Success!')
            else:
                plt.suptitle(f'Episode {self.curr_episode}')
            
            x=[]
            y=[]
            z=[]
            for phase_set in self.phase_volt_set_memory[self.curr_episode]:
                x.append(phase_set[0])
                y.append(phase_set[1])
                z.append(phase_set[2])
            plt.subplot(231)
            plt.ylim((0.94,1.06))
            plt.xlabel('step')
            plt.ylabel('v14 factor')
            plt.plot(z, 'yo-')

            plt.subplot(232)
            plt.ylim((-20,20))
            plt.xlim((-20,20))
            plt.xlabel('p14')
            plt.ylabel('p21') 
            plt.grid()
            vf = self.phase_volt_set_memory[self.curr_episode][-1][2]
            plt.title(f'vf = {vf:4f}')
            plt.plot(x, y, 'go-')
            plt.scatter(x,y, c=self.diff_estimate_memory[self.curr_episode])
            plt.colorbar()
            plt.clim(0, 0.02)

            plt.subplot(233)
            plt.title(f'Loss: {self.diff_estimate:4f}')
            plt.ylim((-0.05,1.0))
            plt.plot(self.profile)

            plt.subplot(234)
            plt.title("Loss")
            plt.plot(self.diff_estimate_memory[self.curr_episode], 'o-')
            length = len(self.diff_estimate_memory[self.curr_episode])
            plt.plot(np.linspace(0,length,length+1),np.ones(length+1)*VOLT_LOSS_CRITERIA, 'k--', label='Stop Criterion')
            # plt.subplot(335)
            # plt.title("tomo acq.")
            # plt.imshow(datamatrix, aspect='auto')
            plt.subplot(235)
            plt.plot(self.bunches[0], label='b1')
            plt.plot(self.bunches[1], label='b2')
            plt.plot(self.bunches[2], label='b3')
            plt.subplot(236)
            plt.ylim((-0.3,0.3))
            plt.plot(self.fwhms-np.mean(self.fwhms), 'go-', label='Rel. fwhms')
            # normalize intensities
            self.intensities = self.intensities/max(self.intensities)
            plt.plot(self.intensities-np.mean(self.intensities), 'bo-', label='Rel. intensity')
            plt.legend()
            

               
            #plot_finish(fig=fig, axes=axes, xlabel='Setting', ylabel='Observable')
            plt.pause(0.1)
        elif mode=='console':
            pass
        
