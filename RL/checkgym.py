from stable_baselines3.common.env_checker import check_env
import gym
import numpy as np
#from quadsplit_class_continuous import QuadSplitContinuous
#from twosplit_class_bunch_lengths import TwoSplitContinuousBL
#from quadsplit_class_bunch_lengths import QuadSplitContinuousBL
#from trisplit_class_bunch_lengths import TriSplitContinuousBL
#from tri_env_bl_int_cnn import TriEnvBLIntCNN
# from tri_env_bl_int_cnn_test_dict import TriEnvBLIntCNN
# from tri_env_profile_cnn import TriEnvProfileCNN
# from tri_env_rel_bl_int_cnn_phase_loss import TriEnvRelBLIntCNNPhase
# from tri_env_rel_bl_int_voltage_loss import TriEnvRelBLIntVoltage
from quad_env_rel_bl_int_42 import QuadEnvBLInt


"""
Script to check whether environment conforms to the gym framework. Useful to test new environment implementations!
"""

env = QuadEnvBLInt()
result = check_env(env)