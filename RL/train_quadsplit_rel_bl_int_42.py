#%%
from os import name
import gym
import numpy as np
from quad_env_rel_bl_int_42 import QuadEnvBLInt
# from quad_env_profiles_cnn import QuadEnvProfilesCNN
# from quad_env_profiles import QuadEnvProfiles

from stable_baselines3 import SAC
from stable_baselines3 import TD3
from stable_baselines3 import DDPG
from stable_baselines3 import her
#from stable_baselines3.common.policies import FeedForwardPolicy
from stable_baselines3.common.noise import NormalActionNoise, OrnsteinUhlenbeckActionNoise
from stable_baselines3.common.vec_env import DummyVecEnv
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.common.results_plotter import load_results, ts2xy
#from stable_baselines3 HerReplayBuffer
import matplotlib.pyplot as plt
from stable_baselines3.common.callbacks import CheckpointCallback
from stable_baselines3.common.callbacks import EvalCallback, StopTrainingOnRewardThreshold

# Save a checkpoint every 1000 steps
# Separate evaluation env
eval_env = QuadEnvBLInt()
#eval_env = QuadEnvProfilesCNN()
# Use deterministic actions for evaluation
callback_on_best = StopTrainingOnRewardThreshold(reward_threshold=101, verbose=1)
#for reward_scale in range(20):
#    ent_coef = 1/(reward_scale+1)
#callback_on_new_best=callback_on_best,
eval_callback = EvalCallback(eval_env, callback_on_new_best=callback_on_best,  best_model_save_path='./RL_logs/quad/',
    log_path='./RL_logs/', eval_freq=500,
    deterministic=True, render=False)


env = QuadEnvBLInt()
#env = QuadEnvProfilesCNN(cnn_model_name='cnn_quad_ADD_NOISEApr_14_2022_11-38-07_e27')
#env = QuadEnvProfilesCNN(cnn_model_name='cnn_quad_ADD_NOISEApr_14_2022_11-38-07_e27')

seed = 7
np.random.seed(seed)
env.seed(seed)
log_dir = './'
#env = Monitor(env, log_dir, allow_early_resets=True)

nb_actions = env.action_space.shape[0]

#env = DummyVecEnv([lambda: env])
# The noise objects for TD3
n_actions = env.action_space.shape[-1]
action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.01 * np.ones(n_actions))
#action_noise = OrnsteinUhlenbeckActionNoise(mean=np.zeros(n_actions), sigma=0.1 * np.ones(n_actions))
#Custom MLP policy
#class CustomPolicy(FeedForwardPolicy):
#    def __init__(self, *args, **kwargs):
#        super(CustomPolicy, self).__init__(*args, **kwargs,
#                                            net_arch=[dict(pi=[8,4,2],
#                                            vf=[8,4,2])],
#                                            feature_extraction="mlp")

#policy_kwargs = dict(net_arch=[64,64])# policy_kwargs=policy_kwargs, buffer_size=5000,
#learning_rate=0.001,
# action_noise=action_noise,
# ent_coef=ent_coef,
ent_coef = 1/5
model = SAC("MlpPolicy", env, verbose=1, ent_coef=ent_coef, learning_starts=100, tensorboard_log="./quadsplit_BL_tensorboard")
print("Starting training...")
model.learn(total_timesteps=50000, callback=eval_callback, log_interval=5, tb_log_name="PLOTS_SAC-Simple-Profile-relBLInt42-step20-diff-00003")
print("Completed training")
model.save("quadsplit_cnn_fixed")
env = model.get_env()

del model # remove to demonstrate saving and loading

#%%

model = SAC.load("./logs/quad/best_model")

obs = env.reset()
performance = [0,0]
predicted_phase_error_when_done = []
for _ in range(500):
    #print(obs)
    action, _states = model.predict(obs)
    #print(action*10)
    obs, rewards, done, info = env.step(action)
    #print(obs)
    #env.render()
    if done:
        print(f"Took {_} steps before terminating test")
        print(f"Info {info}")
        if info['success'] == True:
            performance[0] += 1
        else:
            performance[1] += 1
        predicted_phase_error_when_done.append(info['initial_phase'] + info['phase_corr'])
        obs = env.reset()
print(f"Succesful optimizations: {performance[0]}")
print(f"Unsuccesful optimizations: {performance[1]}")
print(f"Accuracy: {performance[0]/(performance[0]+performance[1])}")

p42_err = []
p84_err = []

for entry in predicted_phase_error_when_done:
    p42_err.append(entry[0])
    p84_err.append(entry[1])
plt.plot(p42_err, p84_err, '.')
plt.show()
# %%
